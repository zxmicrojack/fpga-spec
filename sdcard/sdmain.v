/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
module sdmain(
  output SD_cs,
  output SD_clk,
  output SD_datain,
  input SD_dataout,
  input[31:0] sd_address,
  input sd_rd,
  input sd_wr,
  input rstn,
  input clk_sd,
  output sdcard_ready,
  input sdcard_rfifo_wclk,
  input sdcard_rfifo_rclk,
  input sdcard_rfifo_reset,
  output sdcard_rfifo_empty,
  output sdcard_rfifo_full,
  output[7:0] sdcard_rfifo_data
  );

reg[7:0] sd_din = 8'h00;
reg[7:0] sd_dout = 8'h00;
reg dout_valid = 1'b0;
reg sd_din_valid = 1'b0;
reg sd_din_req = 1'b0;

  sd_controller sd_controller(
    .cs(SD_cs),
    .mosi(SD_datain),
    .miso(SD_dataout),
    .sclk(SD_clk),

    .address(sd_address),
    .rd(sd_rd),
    .wr(sd_wr),
    .dm_in(1'b1),
    .reset(!rstn),
    .din(sd_din),
    .din_valid(sd_din_valid),
    .din_req(sd_din_req),
    .dout(sd_dout),
    .dout_valid(dout_valid),
    .clk(clk_sd),
    .ready(sdcard_ready)
    );

  sdcardram sdcardram(
    .q(sdcard_rfifo_data),
    .d(sd_dout),
    .clkw(dout_valid || sdcard_rfifo_wclk),
    .clkr(sdcard_rfifo_rclk),
    .reset(sdcard_rfifo_reset),
    .empty(sdcard_rfifo_empty),
    .full(sdcard_rfifo_full));



endmodule
