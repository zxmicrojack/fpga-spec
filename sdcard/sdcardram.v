/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define SECTOR_SIZE 1024

module sdcardram(q, d, clkw, clkr, reset, empty, full);
   output[7:0] q;
   input [7:0] d;
   input reset;
   input clkw, clkr;
   output empty;
   output full;

   reg [9:0] addr_in;
   reg [9:0] addr_out;
   reg [9:0] addr_out_reg;

   // reg [7:0] q;
   reg [7:0] mem [0:`SECTOR_SIZE-1] /* synthesis ramstyle = "M144K" */;

   assign empty = addr_in == addr_out;
   assign full = addr_in == 512;

   always @(posedge clkw) begin
     if (reset) begin
       addr_in <= 0;
     end else begin
       mem[addr_in] <= d;
       addr_in <= addr_in + 1;
    end
   end

   always @(posedge clkr) begin
      if (reset) begin
        addr_out <= 0;
        addr_out_reg <= 0;
      end else begin
        addr_out_reg <= addr_out;
        // q <= mem[addr_out_reg];
        // q <= mem[addr_out];
        addr_out <= addr_out + 1;
      end
   end

   assign q = mem[addr_out_reg];


endmodule
