/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define TURBO_1     6
`define TURBO_0     3
`define NORMAL_1     24
`define NORMAL_0     12
`define LEADER_PULSE 30
`define SYNC_PULSE    9

module taploader(
  input [17:0] tap_size,
  output reg[17:0] tap_addr,
  input[7:0] tap_data,
  output reg tap_clk,
  input clk,
  input tap_play,
  output reg ear_in,
  input turbo_loading //
  );

  reg[5:0] tap_pulse_count;
  reg[5:0] tap_pulse_reload;
  reg[12:0] tap_leader_count;
  reg[7:0] tap_data_byte;

  reg tap_output = 0;
  reg[2:0] tap_state;

  reg[15:0] blk_size;


  always @(posedge clk) begin
    parameter IDLE = 0;
    parameter NEW_BLOCK = 1;
    parameter NEW_BLOCK2 = 6;
    parameter LEADER = 2;
    parameter SYNC = 3;
    parameter DATA = 4;

    case (tap_state)
      IDLE:
        if (tap_play) begin
          tap_state <= NEW_BLOCK;
          tap_addr <= 0;
          tap_clk <= 0;
          tap_leader_count <= 0;
          tap_pulse_count <= 0;
          tap_leader_count <= 0;
        end

      NEW_BLOCK:
        if (tap_addr >= tap_size || !tap_play) tap_state <= IDLE;
        else if (tap_clk) begin
          tap_state <= NEW_BLOCK2;
          tap_clk <= 0;
          tap_addr <= tap_addr + 1;
          blk_size[7:0] <= tap_data;
        end else tap_clk <= 1;


      NEW_BLOCK2:
        if (!tap_play) tap_state <= IDLE;
        else if (tap_clk) begin
          tap_state <= LEADER;
          tap_pulse_reload <= `LEADER_PULSE;
          tap_pulse_count <= `LEADER_PULSE;
          tap_leader_count <= 1627*4; // around 4 seconds of leader

          tap_clk <= 0;
          tap_addr <= tap_addr + 1;
          blk_size[15:8] <= tap_data;

          // data[7:0] <= tap_data;
          // // data[7:0] <= tap_state;
          // data_valid_pulse = !data_valid_pulse;
          // data[7:0] <= tap_data;
          // data_valid_pulse = !data_valid_pulse;
        end else tap_clk <= 1;

      LEADER:
        if (!tap_play) tap_state <= IDLE;
        else if (tap_leader_count == 0) begin
          tap_state <= SYNC;
          tap_pulse_reload <= `SYNC_PULSE;
          tap_pulse_count <= `SYNC_PULSE;
          tap_leader_count <= 2;

          // data[7:0] <= tap_state;
          // data_valid_pulse = !data_valid_pulse;
        end

      SYNC:
        if (!tap_play) tap_state <= IDLE;
        else if (tap_leader_count == 0) begin
          tap_state <= DATA;
          if (turbo_loading) begin
            tap_pulse_reload <= tap_data_byte[7] ? `TURBO_1 : `TURBO_0;
            tap_pulse_count <= tap_data_byte[7] ? `TURBO_1 : `TURBO_0;
          end else begin
            tap_pulse_reload <= tap_data_byte[7] ? `NORMAL_1 : `NORMAL_0;
            tap_pulse_count <= tap_data_byte[7] ? `NORMAL_1 : `NORMAL_0;
          end
          tap_leader_count <= 16;

          tap_clk <= 0;
          tap_addr <= tap_addr + 1;
          blk_size <= blk_size - 1;

          // data[7:0] <= tap_state;
          // data_valid_pulse = !data_valid_pulse;
        end else begin
          tap_clk <= 1;
          tap_data_byte <= tap_data;
        end

      DATA:
        if (!tap_play) tap_state <= IDLE;
        else if (tap_leader_count == 0) begin
          if (blk_size == 0) begin
            tap_clk <= 0;
            tap_state <= NEW_BLOCK;
            // data[7:0] <= tap_addr[7:0];
            // data_valid_pulse = !data_valid_pulse;
          end else begin
            tap_clk <= 0;
            tap_addr <= tap_addr + 1;
            blk_size <= blk_size - 1;
            tap_data_byte[7:1] <= tap_data[6:0];

            // mechanism allows only half wave
            tap_leader_count <= 16;
            // TODO: apply 3x loading - is 7/3.5 at 44k1

            if (turbo_loading) begin
              tap_pulse_reload <= tap_data[7] ? `TURBO_1 : `TURBO_0;
              tap_pulse_count <= tap_data[7] ? `TURBO_1 : `TURBO_0;
            end else begin
              tap_pulse_reload <= tap_data[7] ? `NORMAL_1 : `NORMAL_0;
              tap_pulse_count <= tap_data[7] ? `NORMAL_1 : `NORMAL_0;
            end
          end
        end else if (tap_pulse_count == 0 && !tap_leader_count[0]) begin
          // reload next bit - every other pulse
          if (turbo_loading) begin
            tap_pulse_reload <= tap_data_byte[7] ? `TURBO_1 : `TURBO_0;
            tap_pulse_count <= tap_data_byte[7] ? `TURBO_1 : `TURBO_0;
          end else begin
            tap_pulse_reload <= tap_data_byte[7] ? `NORMAL_1 : `NORMAL_0;
            tap_pulse_count <= tap_data_byte[7] ? `NORMAL_1 : `NORMAL_0;
          end
          tap_data_byte[7:1] <= tap_data_byte[6:0];

        end else begin
          tap_clk <= 1;
        end
    endcase

    if (tap_leader_count) begin
      if (!tap_pulse_count) begin
        ear_in <= !ear_in;
        tap_pulse_count <= tap_pulse_reload;
        tap_leader_count <= tap_leader_count - 1;
      end else tap_pulse_count <= tap_pulse_count - 1;
    end

    // CONSTANT LEADER SIGNAL
    // if (!tap_pulse_count) begin
    //   ear_in <= !ear_in;
    //   tap_pulse_count <= 30;
    // end else tap_pulse_count <= tap_pulse_count - 1;
  end

endmodule
