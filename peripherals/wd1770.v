/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define NMI_DELAY 10'h3ff
`define NMI_DELAY_OFF 10'h000

module wd1770(
  // interface to memory module
  input [19:0] disk_size,
  output reg[19:0] disk_addr,
  output reg[7:0] mem_in,
  input[7:0] mem_out,
  output reg mem_rd,
  output reg mem_wr,
  input clk,

  // interface to cpu
  input [7:0] din,
  output reg[7:0] dout,
  input [1:0] a1_0,
  input rd,
  input wr,
  output reg drq //
  );

  reg[7:0] trk;
  reg[7:0] sect;
  reg step_in = 1'b1;
  reg cmd_is_type_1 = 1'b0;
  reg motor_on = 1'b0;
  reg disk_wp = 1'b0;
  reg datamark_n = 1'b0;
  reg lostdata = 1'b0;
  reg[9:0] nmidelay = `NMI_DELAY_OFF;

  parameter CR = {2'b01, 2'b00};
  parameter SR = {2'b10, 2'b00};
  parameter TRKW = {2'b01, 2'b01};
  parameter TRKR = {2'b10, 2'b01};
  parameter SECTW = {2'b01, 2'b10};
  parameter SECTR = {2'b10, 2'b10};
  parameter DATAW = {2'b01, 2'b11};
  parameter DATAR = {2'b10, 2'b11};

  parameter TRK = 2'b01;
  parameter SECT = 2'b10;
  parameter DATA = 2'b11;

  reg crcerror, recnotfound, spinupcomplete;
  wire trk0_n, busy;

  assign trk0_n = trk != 8'h00;
  assign busy = (read_count != 16'h0000) || (write_count != 16'h0000);

  reg[15:0] read_count = 16'h0000;
  reg[15:0] write_count = 16'h0000;
  reg[15:0] read_bytes = 16'h0000;
  reg prev_rd = 1'b0, prev_wr = 1'b0;
  reg[7:0] data_reg = 8'h00;

  always @(posedge clk) begin
    case ({rd, wr, a1_0[1:0]})
      CR: begin
        cmd_is_type_1 <= !din[7];
        casez(din)
          8'b0000????: begin // restore
            // move to track 0 then irq
            // irq <= 1'b1;
            trk <= 8'h00;
            read_count <= 16'h0000;
          end
          8'b0001????: begin // seek
            // move to track stored in data register then update register then irq
            // irq <= 1'b1;
            trk <= data_reg;
          end
          8'b001?????: begin // step b4 is update track reg
            if (step_in && din[4] && trk != 8'hff)
              trk <= trk + 1;
              // {irq, trk} <= {1'b1, trk + 1};
            else if (!step_in && din[4] && trk != 8'h00)
              // {irq, trk} <= {1'b1, trk - 1};
              trk <= trk - 1;
          end
          8'b010?????: begin // step in b4 is update track reg
            if (din[4] && trk != 8'hff)
              {trk, step_in} <= {trk + 1, 1'b1};
              // {irq, trk, step_in} <= {1'b1, trk + 1, 1'b1};
          end
          8'b011?????: begin // step out b4 is update track reg
            if (din[4] && trk != 8'h00)
              {trk, step_in} <= {trk - 1, 1'b0};
              // {irq, trk, step_in} <= {1'b1, trk - 1, 1'b0};
          end
          8'b100?????: begin // read sector
            // if (sect >= 18 || trk >= 40) begin
            //   recnotfound <= 1'b1;
            // end else begin
              // TODO: assumes 18 sectors / track and 256 bytes per sector;
              disk_addr[19:8] <= trk * 18 + sect;
              disk_addr[7:0] <= 8'h00;
              mem_rd <= 1'b1;
              read_count[15:0] <= 16'h100;
              nmidelay <= `NMI_DELAY;
            // end
          end
          8'b101?????: begin // write sector
            // TODO: assumes 18 sectors / track and 256 bytes per sector;
            disk_addr[19:8] <= (trk * 18 + sect) - 1;
            disk_addr[7:0] <= 8'hff;
            write_count[15:0] <= 16'h100;
            nmidelay <= `NMI_DELAY;
          end
          8'b1100????: begin // read address
          end
          8'b1110????: begin // read track
            // TODO: assumes 18 sectors / track and 256 bytes per sector;
            disk_addr[19:8] <= trk * 18;
            disk_addr[7:0] <= 8'h00;
            mem_rd <= 1'b1;
            read_count[15:0] <= 16'h1200;
            nmidelay <= `NMI_DELAY;
          end
          8'b1111????: begin // write track
            disk_addr[19:8] <= (trk * 18) - 1;
            disk_addr[7:0] <= 8'hff;
            write_count[15:0] <= 16'h1200;
            nmidelay <= `NMI_DELAY;
          end
          8'b1101????: begin // force interrupt
            read_count[15:0] <= 16'h0000;
            write_count[15:0] <= 16'h0000;
            nmidelay <= `NMI_DELAY_OFF;
          end
        endcase // casez(din)
      end // CR: begin

      // SR: dout <= 8'b00;
      SR: dout <= cmd_is_type_1 ?
        {motor_on,    1'b0, spinupcomplete, recnotfound, crcerror,   trk0_n, drq, busy} :
        {motor_on, disk_wp,     datamark_n, recnotfound, crcerror, lostdata, drq, busy};
      TRKW: trk <= din;
      TRKR: dout <= trk;
      SECTW: sect <= din;
      SECTR: dout <= sect;
      DATAW: begin
        data_reg <= din;
        if (write_count != 0) begin
          mem_wr <= 1'b1;
          mem_in <= din;
          if (!prev_wr && wr) begin
            write_count <= write_count - 1;
            disk_addr <= disk_addr + 1;
          end
          drq <= 1'b0;
          nmidelay <= write_count != 0 ? `NMI_DELAY : `NMI_DELAY_OFF;
        end
      end
      DATAR: begin
        if (read_count != 0) begin
          mem_rd <= 1'b1;
          dout <= mem_out;
          if (!prev_rd && rd) begin
            read_count <= read_count - 1;
            read_bytes <= read_bytes + 1;
            disk_addr <= disk_addr + 1;
          end
          drq <= 1'b0;
          nmidelay <= read_count != 0 ? `NMI_DELAY : `NMI_DELAY_OFF;
        end
      end

      default: begin
        {mem_rd, mem_wr} <= 2'b00;
        if (nmidelay != 10'h000) nmidelay <= nmidelay - 1;
        if (nmidelay == 10'h001 && busy) drq <= 1'b1;
      end

    endcase

    prev_rd <= rd;
    prev_wr <= wr;
  end

endmodule
