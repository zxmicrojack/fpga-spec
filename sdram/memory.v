/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
module memory(
  input clk50m,
  output [`NR_NODES*8-1:0] sdram_dout_m,
  input [`NR_NODES*8-1:0] sdram_din_m,
  input [`NR_NODES*25-1:0] sdram_addr_m,
  input [`NR_NODES-1:0] sdram_rd_m,
  input [`NR_NODES-1:0] sdram_we_m,
  inout  [15:0] SDRAM_DQ,    // 16 bit bidirectional data bus
  output [12:0] SDRAM_A,     // 13 bit multiplexed address bus
  output         SDRAM_DQML,  // two byte masks
  output         SDRAM_DQMH,  //
  output   [1:0] SDRAM_BA,    // two banks
  output            SDRAM_nCS,   // a single chip select
  output            SDRAM_nWE,   // write enable
  output            SDRAM_nRAS,  // row address select
  output            SDRAM_nCAS,  // columns address select
  output            SDRAM_CKE,   // clock enable
 output            SDRAM_CLK //
);


  wire sdram_clk;

  PLL_SDRAM sdram_pll0_inst(
    .areset(1'b0),
    .inclk0(clk50m),
    .c0(sdram_clk),
    // .c1(sdram_clk),
    .locked()
    );


  wire [7:0] sdram_dout;        // data output to cpu
  wire sdram_ready;        // dout is valid. Ready to accept new read/write.

  wire [24:0] sdram_addr;        // 25 bit address for 8bit mode. addr[0] = 0 for 16bit mode for correct operations.
  wire [7:0] sdram_din;         // data input from cpu
  wire sdram_we;          // cpu requests write
  wire sdram_rd;          // cpu requests read

  sdram sdram(
    // initial system stuff
    .init(1'b0),
    .clk(sdram_clk),

    // signal pins
    .SDRAM_DQ(SDRAM_DQ),
    .SDRAM_A(SDRAM_A),
    .SDRAM_DQML(SDRAM_DQML),
    .SDRAM_DQMH(SDRAM_DQMH),
    .SDRAM_BA(SDRAM_BA),
    .SDRAM_nCS(SDRAM_nCS),
    .SDRAM_nWE(SDRAM_nWE),
    .SDRAM_nRAS(SDRAM_nRAS),
    .SDRAM_nCAS(SDRAM_nCAS),
    .SDRAM_CKE(SDRAM_CKE),
    .SDRAM_CLK(SDRAM_CLK),

    // module interface
    .addr(sdram_addr),
    .dout(sdram_dout),
    .din(sdram_din),
    .we(sdram_we),
    .rd(sdram_rd),
    .ready(sdram_ready)
  );


  sdrammux sdrammux(
    .clk50m(clk50m),
    .sdram_dout_m(sdram_dout_m),
    .sdram_din_m(sdram_din_m),
    .sdram_addr_m(sdram_addr_m),
    .sdram_rd_m(sdram_rd_m),
    .sdram_we_m(sdram_we_m),
    .sdram_addr(sdram_addr),
    .sdram_dout(sdram_dout),
    .sdram_din(sdram_din),
    .sdram_we(sdram_we),
    .sdram_rd(sdram_rd),
    .sdram_ready(sdram_ready)
    );

endmodule
