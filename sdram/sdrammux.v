/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define NR_NODES 3

module sdrammux(
  input clk50m,
  output reg [`NR_NODES*8-1:0] sdram_dout_m,
  input [`NR_NODES*8-1:0] sdram_din_m,
  input [`NR_NODES*25-1:0] sdram_addr_m,
  input [`NR_NODES-1:0] sdram_rd_m,
  input [`NR_NODES-1:0] sdram_we_m,

  output reg [24:0] sdram_addr,
  input [7:0] sdram_dout,
  output reg [7:0] sdram_din,
  output reg sdram_we,
  output reg sdram_rd,
  input sdram_ready //
  );

  reg sdram_we_prev_m[3];
  reg sdram_rd_prev_m[3];
  reg[1:0] sdram_chan;

  reg[2:0] sdram_mpe_state = SME_IDLE;

  parameter SME_IDLE = 0;
  parameter SVC_READ = 1;
  parameter SVC_WRITE = 2;

  always @(posedge clk50m) begin
    case (sdram_mpe_state)
      SME_IDLE: begin
        // TODO is there a cleaner way of doing this?
        if (sdram_we_m[0] != sdram_we_prev_m[0] || sdram_rd_m[0] != sdram_rd_prev_m[0]) begin
          sdram_we_prev_m[0] <= sdram_we_m[0];
          sdram_rd_prev_m[0] <= sdram_rd_m[0];

          if (sdram_we_m[0] || sdram_rd_m[0]) begin
            sdram_chan <= 0;
            sdram_addr <= sdram_addr_m[24:0];
            sdram_din <= sdram_din_m[7:0];
            sdram_we <= sdram_we_m[0];
            sdram_rd <= sdram_rd_m[0];
            sdram_mpe_state <= sdram_rd_m[0] ? SVC_READ : SVC_WRITE;
          end
        end else if (sdram_we_m[1] != sdram_we_prev_m[1] || sdram_rd_m[1] != sdram_rd_prev_m[1]) begin
          sdram_we_prev_m[1] <= sdram_we_m[1];
          sdram_rd_prev_m[1] <= sdram_rd_m[1];

          if (sdram_we_m[1] || sdram_rd_m[1]) begin
            sdram_chan <= 1;
            sdram_addr <= sdram_addr_m[49:25]; // 24..0
            sdram_din <= sdram_din_m[15:8];
            sdram_we <= sdram_we_m[1];
            sdram_rd <= sdram_rd_m[1];
            sdram_mpe_state <= sdram_rd_m[1] ? SVC_READ : SVC_WRITE;
          end

        end else if (sdram_we_m[2] != sdram_we_prev_m[2] || sdram_rd_m[2] != sdram_rd_prev_m[2]) begin
          sdram_we_prev_m[2] <= sdram_we_m[2];
          sdram_rd_prev_m[2] <= sdram_rd_m[2];

          if (sdram_we_m[2] || sdram_rd_m[2]) begin
            sdram_chan <= 2;
            sdram_addr <= sdram_addr_m[74:50]; // 24..0
            sdram_din <= sdram_din_m[23:16];
            sdram_we <= sdram_we_m[2];
            sdram_rd <= sdram_rd_m[2];
            sdram_mpe_state <= sdram_rd_m[2] ? SVC_READ : SVC_WRITE;
          end
        end
      end

      SVC_READ: begin
        if (sdram_ready) begin
          // sdram_dout_m_[sdram_chan*8+:8] <= sdram_dout;
          sdram_dout_m[sdram_chan*8+:8] <= sdram_dout;
          sdram_mpe_state <= SME_IDLE;
          sdram_rd <= 1'b0;
        end
      end

      SVC_WRITE: begin
        if (sdram_ready) begin
          sdram_mpe_state <= SME_IDLE;
          sdram_we <= 1'b0;
        end
      end

      default: sdram_mpe_state <= SME_IDLE;

    endcase
  end



endmodule
