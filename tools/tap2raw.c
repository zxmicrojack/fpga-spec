/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
#include <stdio.h>
#include <stdint.h>

uint8_t data[65536];

// #define NORMAL_1     12
// #define NORMAL_0     6
// 1x
// #define NORMAL_1     48
// #define NORMAL_0     24
// 2x
// #define NORMAL_1     24
// #define NORMAL_0     12
// 4x
// #define NORMAL_1     12
// #define NORMAL_0     6
// 8x
#define NORMAL_1     10
#define NORMAL_0     6


#define LEADER_PULSE 60
#define SYNC_PULSE   18

#define LEADER_LEN    1627*2

void outh(FILE *f, int pulse) {
  int q = pulse/2;
  while (q--) fputc(0x80, f);
  q = pulse/2;
  while (q--) fputc(0x7f, f);
}

int main(int argc, char **argv) {
  FILE *fin = fopen(argv[1], "rb");
  if (fin) {
    FILE *fout = fopen(argv[2], "wb");

    while (!feof(fin)) {
      uint16_t len;

      if (fread(&len, 1, sizeof len, fin) != sizeof len) {
        printf("Failed to read 2 bytes\n");
        break;
      }

      if (fread(data, 1, len, fin) != len) {
        printf("Failed to read %d bytes\n", len);
        break;
      }

      if (len == 6914) { // just do screen for now
        int i, j;
        uint8_t d;

        for (i=0; i<LEADER_LEN; i++) {
          outh(fout, LEADER_PULSE);
        }
        outh(fout, SYNC_PULSE);
        for (i=0; i<len; i++) {
          d = data[i];
          for (j=0; j<8; j++) {
            outh(fout, (d & 0x80) ? NORMAL_1 : NORMAL_0);
            d <<= 1;
          }
        }
      }

      printf("block len %d\n", len);
    }

    fclose(fout);
    fclose(fin);
  }
  return 0;
}
