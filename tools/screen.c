/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
#include <SDL/SDL.h>
#include <SDL/SDL_video.h>
#include "Z80.h"

#include <stdio.h>

SDL_Surface *screen;
extern byte mem[0x10000];

unsigned long zxColours[] = {
  0x000000, 0x0000d7, 0xd70000, 0xd700d7,
  0x00d700, 0x00d7d7, 0xd7d700, 0xd7d7d7,
  0x000000, 0x0000ff, 0xff0000, 0xff00ff,
  0x00ff00, 0x00ffff, 0xffff00, 0xffffff
};


Uint32 colourLut[16];

void initScreen() {
  /* Initialize the SDL library */
  if( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
     fprintf(stderr,
             "Couldn't initialize SDL: %s\n", SDL_GetError());
     exit(1);
  }

  /* Clean up on exit */
  atexit(SDL_Quit);
  /*
  * Initialize the display in a 640x480 8-bit palettized mode,
  * requesting a software surface
  */
  screen = SDL_SetVideoMode(640, 480, 8, SDL_SWSURFACE);
  if ( screen == NULL ) {
     fprintf(stderr, "Couldn't set 640x480x8 video mode: %s\n",
                     SDL_GetError());
     exit(1);
  }

  for (int i=0; i<sizeof zxColours / sizeof zxColours[0]; i++) {
    /* Map the color yellow to this display (R=0xff, G=0xFF, B=0x00)
       Note:  If the display is palettized, you must set the palette first.
    */
    colourLut[i] = SDL_MapRGB(screen->format, zxColours[i]>>16,
      (zxColours[i]>>8) & 0xff, zxColours[i] & 0xff);
  }
}

/*
 * Set the pixel at (x, y) to the given value
 * NOTE: The surface must be locked before calling this!
 */
void putpixel_ll(SDL_Surface *surface, int x, int y, Uint32 pixel)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to set */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        *p = pixel;
        break;

    case 2:
        *(Uint16 *)p = pixel;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            p[0] = (pixel >> 16) & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = pixel & 0xff;
        } else {
            p[0] = pixel & 0xff;
            p[1] = (pixel >> 8) & 0xff;
            p[2] = (pixel >> 16) & 0xff;
        }
        break;

    case 4:
        *(Uint32 *)p = pixel;
        break;
    }
}

void putpixel(SDL_Surface *surface, int x, int y, Uint32 pixel) {
  putpixel_ll(surface, x*2, y*2, pixel);
  putpixel_ll(surface, x*2+1, y*2, pixel);
  putpixel_ll(surface, x*2, y*2+1, pixel);
  putpixel_ll(surface, x*2+1, y*2+1, pixel);
}


void updateScreen() {
  /* Lock the screen for direct access to the pixels */
  if ( SDL_MUSTLOCK(screen) ) {
      if ( SDL_LockSurface(screen) < 0 ) {
          fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
          return;
      }
  }
  for (int i=191; i>=0; i--) {
    for (int j=0; j<32; j++) {
      int l = (i/64) * 2048;
      l += (i & 7) * 256;
      l += ((i & 63) / 8) * 32;

      byte d = mem[16384+l+j];
      int y = i / 8;
      byte attr = mem[16384+2048*3+y*32+j];
      Uint32 rgbInk = colourLut[(attr & 7) + ((attr&0x40) ? 8 : 0)];// ^ 0xffffff;
      Uint32 rgbPaper = colourLut[((attr >> 3) & 7) + ((attr&0x40) ? 8 : 0)];// ^ 0xffffff;
      for (int k=0; k<8; k++) {
        putpixel(screen, j*8+k, i, (d & 0x80) ? rgbInk : rgbPaper);
        d <<= 1;
      }
    }
  }

  if ( SDL_MUSTLOCK(screen) ) {
      SDL_UnlockSurface(screen);
  }

  /* Update just the part of the display that we've changed */
  SDL_UpdateRect(screen, 0, 0, 256*2, 192*2);
}


void example() {

    /* Code to set a yellow pixel at the center of the screen */

    int x, y;
    Uint32 yellow;

    /* Map the color yellow to this display (R=0xff, G=0xFF, B=0x00)
       Note:  If the display is palettized, you must set the palette first.
    */
    yellow = SDL_MapRGB(screen->format, 0xff, 0xff, 0x00);

    x = screen->w / 2;
    y = screen->h / 2;

    /* Lock the screen for direct access to the pixels */
    if ( SDL_MUSTLOCK(screen) ) {
        if ( SDL_LockSurface(screen) < 0 ) {
            fprintf(stderr, "Can't lock screen: %s\n", SDL_GetError());
            return;
        }
    }

    putpixel(screen, x, y, yellow);

    if ( SDL_MUSTLOCK(screen) ) {
        SDL_UnlockSurface(screen);
    }
    /* Update just the part of the display that we've changed */
    SDL_UpdateRect(screen, x, y, 1, 1);

    return;
}
