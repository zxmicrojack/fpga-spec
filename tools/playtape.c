/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "Z80.h"

#define MAX_SAMPLES 2000000

byte mem[0x10000];
// char wave[1574599];
char *wave;
unsigned long waveSize = 0;
double speed = 1.0;

int saveMode = 0;

#define BMPSIZE 147510

byte bmpHeader[] = {
  0x42, 0x4d, 0x36, 0x40, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x36, 0x00, 0x00, 0x00, 0x28, 0x00,
  0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x01, 0x00, 0x18, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x40, 0x02, 0x00, 0x23, 0x2e, 0x00, 0x00, 0x23, 0x2e, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

byte *readIn(const char *file, byte *buffer, int max, unsigned long *actual) {
  FILE *f = fopen(file, "rb");
  if (f) {
    fseek(f, 0, SEEK_END);
    unsigned long size = ftell(f);
    fseek(f, 0, SEEK_SET);

    int toRead = max > size ? size : max;

    if (buffer == NULL) {
      buffer = (byte *)malloc(toRead);
    }
    int l = fread(buffer, 1, toRead, f);
    fclose(f);
    printf("Read %d bytes from %s\n", l, file);

    if (actual != NULL) *actual = l;

    return buffer;
  } else {
    printf("Couldn't open file %s\n", file);
  }
  return NULL;
}

// 3494400 TSTATES / SECOND
  // Z80_ICount

#define TSS 3494400

unsigned long tstates = 0;

byte Z80_In (byte Port) {
  Z80_Regs regs;
  Z80_GetRegs(&regs);

  unsigned port = (regs.AF.W.l) | Port;
  unsigned long realTstates = tstates + Z80_IPeriod - Z80_ICount;
  unsigned long sampleNr = (unsigned)(((double)realTstates*44100.0*speed)/(double)TSS);

  if (!saveMode) {
    if (sampleNr < waveSize) {
      byte output = 0xbf | ((wave[sampleNr] > 0) ? 0x40 : 0x00);
      // printf("sampleNr %u out %02X sample %d\n", sampleNr, output, wave[sampleNr]);
      return output;
      // return 0xbf | ((wave[sampleNr] > 0) ? 0x40 : 0x00);
    } else {
      return 0xff;
    }
  } else {
    return 0xff;
  }

  return 0;
}

/****************************************************************************/
/* Output a byte to given I/O port                                          */
/****************************************************************************/
int lastSample = -1;
byte gotit[256] = {0};
void Z80_Out (byte Port,byte Value) {
  // printf("out(port = %02X, value = %02X)\n", Port, Value);
  // if (Port >= 0x20 && Port < 0xf0) {
  // }
  // if (!gotit[Value]) {
  //   printf("out(port = %02X, value = %02X)\n", Port, Value);
  //   gotit[Value] = 1;
  // }

  unsigned long realTstates = tstates + Z80_IPeriod - Z80_ICount;
  unsigned long sampleNr = (unsigned)(((double)realTstates*44100.0*speed)/(double)TSS);
  if (saveMode && sampleNr < MAX_SAMPLES) {
    if (lastSample > 0) {
      lastSample ++;
      while (lastSample < sampleNr) {
        wave[lastSample] = wave[lastSample-1];
        lastSample++;
      }
      wave[sampleNr] = (Value & 0x08) ? 0x7f : 0x80;
      lastSample = sampleNr;
    }
    lastSample = sampleNr;
  }


}

/****************************************************************************/
/* Read a byte from given memory location                                   */
/****************************************************************************/
unsigned Z80_RDMEM(dword A) {
  // printf("readmem(addr = %04X\n", A);
  return mem[A];
}

/****************************************************************************/
/* Write a byte to given memory location                                    */
/****************************************************************************/
void Z80_WRMEM(dword A,byte V) {
  // if (A >= 16384 && A < (16384+6912)) {
  //   printf("writemem(addr = %04X, value = %02X)\n", A, V);
  // }
  if (A < 16384) {
    printf("[Warning] Trying to write to location 0x%04X\n", A);
  } else {
    mem[A] = V;
  }
}

void Z80_Debug(Z80_Regs *R) {}
void Z80_Reti (void) {
  Z80_Running = 0;
}
void Z80_Retn (void) {}

int Z80_IRQ = 0;

int Z80_Interrupt(void) {
  return Z80_IGNORE_INT;
}

void Z80_Patch (Z80_Regs *Regs) {
  // disk access?
}

void writeScreen(const char *filename) {
  byte *bmp = (byte *) malloc(BMPSIZE);
  memcpy(bmp, bmpHeader, sizeof bmpHeader);
  int p=sizeof bmpHeader;

  unsigned long colourLut[] = {
    0x000000, 0x0000d7, 0xd70000, 0xd700d7,
    0x00d700, 0x00d7d7, 0xd7d700, 0xd7d7d7,
    0x000000, 0x0000ff, 0xff0000, 0xff00ff,
    0x00ff00, 0x00ffff, 0xffff00, 0xffffff
  };

  for (int i=191; i>=0; i--) {
    for (int j=0; j<32; j++) {
      int l = (i/64) * 2048;
      l += (i & 7) * 256;
      l += ((i & 63) / 8) * 32;

      byte d = mem[16384+l+j];
      int y = i / 8;
      byte attr = mem[16384+2048*3+y*32+j];
      unsigned long rgbInk = colourLut[(attr & 7) + ((attr&0x40) ? 8 : 0)];// ^ 0xffffff;
      unsigned long rgbPaper = colourLut[((attr >> 3) & 7) + ((attr&0x40) ? 8 : 0)];// ^ 0xffffff;
      for (int k=0; k<8; k++) {
        unsigned long rgb = (d & 0x80) ? rgbInk : rgbPaper;
        bmp[p++] = rgb & 0xff;
        bmp[p++] = (rgb >> 8) & 0xff;
        bmp[p++] = rgb >> 16;

        d <<= 1;
      }
    }
  }

  FILE *f = fopen(filename, "wb");
  if (f) {
    fwrite(bmp, 1, BMPSIZE, f);
    fclose(f);
  }
  free(bmp);
}

void writeOut(const char *filename, void *data, int len) {
  FILE *f = fopen(filename, "wb");
  if (f) {
    fwrite(data, 1, len, f);
    fclose(f);
  }
}

void doTapFile(const char *tapefile) {
  FILE *f = fopen(tapefile, "rb");
  if (f) {
    unsigned short len;
    fread(&len, 1, sizeof len, f);
    printf("%d\n", len);

    fclose(f);
  }
}

int main(int argc, char **argv) {
  if (argc < 5) {
    printf("Usage: tape <tapefile>\n");
    return 1;
  }

  speed = argc > 7 ? atof(argv[6]) : 1.0;
  dword location = atoi(argv[4]);
  if (strcmp(argv[2], "-")) readIn(argv[2], mem, 16384, NULL);
  readIn(argv[3], &mem[location], 1024, NULL);

  if (!strcmp(argv[1], "load")) {
    wave = readIn(argv[5], NULL, MAX_SAMPLES, &waveSize);
  } else if (!strcmp(argv[1], "save")) {
    int loc = 16384;
    readIn(argv[5], &mem[loc], 65536-loc, NULL);
    wave = malloc(MAX_SAMPLES);
    saveMode = 1;
  }

  Z80_Regs regs;
  Z80_GetRegs(&regs);
  regs.PC.D = location;
  Z80_SetRegs(&regs);

  while (Z80_Execute()) {
    tstates += Z80_IPeriod;

    if (tstates > 500000000) {
      break;
    }
  }

  Z80_RegisterDump();
  printf("Terminated after %lu tstates\n", tstates + Z80_IPeriod - Z80_ICount);
  printf("Speed is set to x %.3f", speed);

  if (!strcmp(argv[1], "load")) {
    writeScreen("output.bmp");
    writeOut("output.bin", &mem[16384], 6912);
    free(wave);
  } else if (!strcmp(argv[1], "save")) {
    writeOut("output.raw", wave, lastSample+1);
    free(wave);
  }

  return 0;
}
