/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
#include <stdio.h>
#include <stdint.h>

uint8_t data[65536];

// #define NORMAL_1     12
// #define NORMAL_0     6
// 1x
// #define NORMAL_1     48
// #define NORMAL_0     24
// 2x
// #define NORMAL_1     24
// #define NORMAL_0     12
// 4x
// #define NORMAL_1     12
// #define NORMAL_0     6
// 8x
#define NORMAL_1     10
#define NORMAL_0     6


#define LEADER_PULSE 60
#define SYNC_PULSE   18

#define LEADER_LEN    1627*2

void outh(FILE *f, int pulse) {
  int q = pulse/2;
  while (q--) fputc(0x80, f);
  q = pulse/2;
  while (q--) fputc(0x7f, f);
}

int main(int argc, char **argv) {
  char buf[255];
  int blknr = 0;

  FILE *fin = fopen(argv[1], "rb");
  FILE *fout;

  if (fin) {

    while (!feof(fin)) {
      uint16_t len;

      if (fread(&len, 1, sizeof len, fin) != sizeof len) {
        printf("Failed to read 2 bytes\n");
        break;
      }

      if (fread(data, 1, len, fin) != len) {
        printf("Failed to read %d bytes\n", len);
        break;
      }

      sprintf(buf, "block%d-%02X.bin", blknr++, data[0]);
      fout = fopen(buf, "wb");

      fwrite(data+1, 1, len-2, fout);
      fclose(fout);
    }

    fclose(fout);
    fclose(fin);
  }
  return 0;
}
