/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`timescale 1ns / 1ps
`define ON_TIME 4'd7
`define OFF_TIME 4'd15

module aud_test (
                  input aud_clk,
                  output reg[15:0] dac_out_l
             );

  // reg[15:0] dac_out_l;
  //

  reg [15:0] SINE [0:55];
  initial begin
    $readmemh("sine.hex", SINE);
  end

  reg [7:0] dac_count;
  initial dac_count = 0;
  always @(posedge aud_clk)
    if (dac_count <= 55) begin
      dac_out_l[15:0] <= SINE[dac_count];
      dac_count <= dac_count + 1;
    end else if (dac_count <= 110) begin
      dac_out_l[15:0] <= 16'h8000 - SINE[dac_count];
      dac_count <= dac_count + 1;
    end else begin
      dac_count <= 0;
      dac_out_l[15:0] <= SINE[0];
    end
endmodule
