/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
module audio(
  output aud_left,
  output aud_right,
  input clk50m,
  input clk_audio,
  input rstn,

  input ym2149_clk,
  input[7:0] ym2149_di,
  output[7:0] ym2149_do,
  input ym2149_bdir,
  input ym2149_bc,

  input ear,
  input RESET //
  );

  // generate sine wave
  // aud_test aud_test(
  //   .aud_clk(count[9]),
  //   .dac_out_l(dac_out_l)
  //   );

  // audio dacs
  reg [15:0] dac_out_l;
  wire[7:0] ym2149_cha_out, ym2149_chb_out, ym2149_chc_out;

  dsdac1o dsdac1ol(
    .DACout(aud_left),
    .DACin(dac_out_l),
    .CLK(clk50m),
    .RESET(!rstn)
    );

  dsdac1o dsdac1or(
    .DACout(aud_right),
    .DACin(dac_out_l),
    .CLK(clk50m),
    .RESET(!rstn)
    );

  ym2149 ym2149(
      .CLK(ym2149_clk),
      .CE(1'b1),
      .RESET(RESET),
      .BDIR(ym2149_bdir),
      .BC(ym2149_bc),
      .DI(ym2149_di),
      .DO(ym2149_do),
      .CHANNEL_A(ym2149_cha_out),
      .CHANNEL_B(ym2149_chb_out),
      .CHANNEL_C(ym2149_chc_out),
      .SEL(1'b1),
      .MODE(1'b1),
      .IOA_in(8'h00),
      .IOB_in(8'h00));

  wire[9:0] ym2149_aud_out;
  assign ym2149_aud_out =
    {2'b00, ym2149_cha_out} +
    {2'b00, ym2149_chb_out} +
    {2'b00, ym2149_chc_out};

  always @(posedge clk_audio)
    dac_out_l <= (ear ? 16'hc000 : 16'h4000) + {ym2149_aud_out,6'h00};

endmodule
