
; 1x
#define SA_BIT_1_K    $42
#define SA_OUT_K    $3e
#define SA_8_BITS_K   $31

; 2x
;#define SA_BIT_1_K    $21
;#define SA_OUT_K    $1d
;#define SA_8_BITS_K   $12
;#define SA_8_BITS_K   $10

; 3x
;#define SA_BIT_1_K    $14
;#define SA_OUT_K    $10
;#define SA_8_BITS_K   $3

  org 32768
extern _main

._main
  ld ix,16384
  ld de,6912
  ld a,0xFF
  scf
  call SA_BYTES
  jr c,ERROR
  ld a,0x00
  jr CONTINUE

.ERROR
  ld a,0x01

.CONTINUE
  reti

.SA_BYTES
      ld     hl,SA_LD_RET        ; 0004C2 21 3F 05
      push   hl              ; 0004C5 E5
      ld     hl,$1f80        ; 0004C6 21 80 1F
      bit    7,a             ; 0004C9 CB 7F
      jr     z,SA_FLAG         ; 0004CB 28 03
      ld     hl,$0c98        ; 0004CD 21 98 0C
.SA_FLAG
      ex     af,af'          ; 0004D0 08
      inc    de              ; 0004D1 13
      dec    ix              ; 0004D2 DD 2B
      di                     ; 0004D4 F3
      ld     a,$02           ; 0004D5 3E 02
      ld     b,a             ; 0004D7 47
.SA_LEADER
      djnz   SA_LEADER           ; 0004D8 10 FE
      out    ($fe),a         ; 0004DA D3 FE
      xor    $0f             ; 0004DC EE 0F
      ld     b,$a4           ; 0004DE 06 A4
      dec    l               ; 0004E0 2D
      jr     nz,SA_LEADER        ; 0004E1 20 F5
      dec    b               ; 0004E3 05
      dec    h               ; 0004E4 25
      jp     p,SA_LEADER         ; 0004E5 F2 D8 04

      ld     b,$2f           ; 0004E8 06 2F
.SA_SYNC1
      djnz   SA_SYNC1           ; 0004EA 10 FE
      out    ($fe),a         ; 0004EC D3 FE
      ld     a,$0d           ; 0004EE 3E 0D
      ld     b,$37           ; 0004F0 06 37

.SA_SYNC2
      djnz   SA_SYNC2           ; 0004F2 10 FE
      out    ($fe),a         ; 0004F4 D3 FE
      ld     bc,$3b0e        ; 0004F6 01 0E 3B
      ex     af,af'          ; 0004F9 08
      ld     l,a             ; 0004FA 6F
      jp     SA_START           ; 0004FB C3 07 05
.SA_LOOP
      ld     a,d             ; 0004FE 7A
      or     e               ; 0004FF B3
      jr     z,SA_PARITY         ; 000500 28 0C
      ld     l,(ix+$00)      ; 000502 DD 6E 00
.SA_LOOP_P
      ld     a,h             ; 000505 7C
      xor    l               ; 000506 AD
.SA_START
      ld     h,a             ; 000507 67
      ld     a,$01           ; 000508 3E 01
      scf                    ; 00050A 37
      jp     SA_8_BITS           ; 00050B C3 25 05
.SA_PARITY
      ld     l,h             ; 00050E 6C
      jr     SA_LOOP_P           ; 00050F 18 F4
.SA_BIT_2
      ld     a,c             ; 000511 79
      bit    7,b             ; 000512 CB 78
.SA_BIT_1
      djnz   SA_BIT_1           ; 000514 10 FE
      jr     nc,SA_OUT        ; 000516 30 04

;      ld     b,$42           ; 000518 06 42
;      ld     b,$21           ; 000518 06 42
      ld     b,SA_BIT_1_K           ; 000518 06 42
.SA_SET
      djnz   SA_SET           ; 00051A 10 FE
.SA_OUT
      out    ($fe),a         ; 00051C D3 FE

      ld      b,SA_OUT_K
;      ld     b,$1d           ; 00051E 06 3E

;      ld     b,$3e           ; 00051E 06 3E
      jr     nz,SA_BIT_2        ; 000520 20 EF
      dec    b               ; 000522 05
      xor    a               ; 000523 AF
      inc    a               ; 000524 3C
.SA_8_BITS
      rl     l               ; 000525 CB 15
      jp     nz,SA_BIT_1        ; 000527 C2 14 05
      dec    de              ; 00052A 1B
      inc    ix              ; 00052B DD 23

;      ld     b,$31           ; 00052D 06 31
;      ld     b,$10           ; 00052D 06 31
      ld     b,SA_8_BITS_K

      ld     a,$7f           ; 00052F 3E 7F
      in     a,($fe)         ; 000531 DB FE
      rra                    ; 000533 1F
      ret    nc              ; 000534 D0
      ld     a,d             ; 000535 7A
      inc    a               ; 000536 3C
      jp     nz,SA_LOOP        ; 000537 C2 FE 04
      ld     b,$3b           ; 00053A 06 3B
.SA_DELAY
      djnz   SA_DELAY           ; 00053C 10 FE
      ret                    ; 00053E C9

.SA_LD_RET
      push   af              ; 00053F F5
      ld     a,($5c48)       ; 000540 3A 48 5C
      and    $38             ; 000543 E6 38
      rrca                   ; 000545 0F
      rrca                   ; 000546 0F
      rrca                   ; 000547 0F
      out    ($fe),a         ; 000548 D3 FE
      ld     a,$7f           ; 00054A 3E 7F
      in     a,($fe)         ; 00054C DB FE
      rra                    ; 00054E 1F
      ei                     ; 00054F FB
      jr     c,SA_LD_END         ; 000550 38 02
.REPORT_D
      rst    08h             ; 000552 CF
      inc    c               ; 000553 0C
.SA_LD_END
      pop    af              ; 000554 F1
      ret                    ; 000555 C9
