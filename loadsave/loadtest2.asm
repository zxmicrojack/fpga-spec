#define BORDCR $5c48

  org 32768
extern _main

._main
  ld ix,16384
  ld de,6912
  ld a,0xFF
  scf
  call LD_BYTES
  jr c,ERROR
  ld a,0x00
  jr CONTINUE

.ERROR
  ld a,0x01

.CONTINUE
  reti

#define LD_WAIT_1_TIME $0415
;#define EDGE_SAMPLING_TIME $16
#define LEADER_TIMING_K $9c
#define LEADER_TIMING_K2 $c6
#define LEADER_TIMING_K3 $c9
#define OFF_SYNC_PULSE $d4
#define FLAG_TIMING_K $b0
#define DATA_TIMING_K $b0

; 1x
;#define BIT_THRESHOLD $cb
;#define EDGE_SAMPLING_TIME $16
; 2x low 0xbc high 0xc7
;#define BIT_THRESHOLD $c1
;#define EDGE_SAMPLING_TIME $2
; 4x low 0xb7 high 0xbb
;#define BIT_THRESHOLD $b9
;#define EDGE_SAMPLING_TIME $2
; 5x low 0xb4 high 0xb7
#define BIT_THRESHOLD $b5
#define EDGE_SAMPLING_TIME $1


; 053f
.SA_LD_RET
      push   af              ; 000000 F5
      ld     a,(BORDCR)       ; 000001 3A 48 5C
      and    $38             ; 000004 E6 38
      rrca                   ; 000006 0F
      rrca                   ; 000007 0F
      rrca                   ; 000008 0F
      out    ($fe),a         ; 000009 D3 FE
      ld     a,$7f           ; 00000B 3E 7F
      in     a,($fe)         ; 00000D DB FE
      rra                    ; 00000F 1F
      ei                     ; 000010 FB
      jr     c,SA_LD_END     ; 000011 38 02

.REPORT_D
      rst    08h             ; 000013 CF
      defb   $0c             ; 000014 0C

.SA_LD_END
      pop    af              ; 000015 F1
      ret                    ; 000016 C9

.LD_BYTES
      inc    d               ; 000017 14
      ex     af,af'          ; 000018 08
      dec    d               ; 000019 15
      di                     ; 00001A F3
      ld     a,$0f           ; 00001B 3E 0F
      out    ($fe),a         ; 00001D D3 FE
      ld     hl,SA_LD_RET    ; 00001F 21 3F 05
      push   hl              ; 000022 E5
      in     a,($fe)         ; 000023 DB FE
      rra                    ; 000025 1F
      and    $20             ; 000026 E6 20
      or     $02             ; 000028 F6 02
      ld     c,a             ; 00002A 4F
      cp     a               ; 00002B BF

.LD_BREAK
      ret    nz              ; 00002C C0
.LD_START
      call   LD_EDGE_1       ; 00002D CD E7 05
      jr     nc,LD_BREAK     ; 000030 30 FA
      ld     hl,LD_WAIT_1_TIME        ; 000032 21 15 04
.LD_WAIT
      djnz   LD_WAIT           ; 000035 10 FE
      dec    hl              ; 000037 2B
      ld     a,h             ; 000038 7C
      or     l               ; 000039 B5
      jr     nz,LD_WAIT        ; 00003A 20 F9
      call   LD_EDGE_2           ; 00003C CD E3 05
      jr     nc,LD_BREAK       ; 00003F 30 EB
.LD_LEADER
      ld     b,LEADER_TIMING_K           ; 000041 06 9C
      call   LD_EDGE_2           ; 000043 CD E3 05
      jr     nc,LD_BREAK        ; 000046 30 E4
      ld     a,LEADER_TIMING_K2           ; 000048 3E C6
      cp     b               ; 00004A B8
      jr     nc,LD_START        ; 00004B 30 E0
      inc    h               ; 00004D 24
      jr     nz,LD_LEADER        ; 00004E 20 F1

.LD_SYNC
      ld     b,LEADER_TIMING_K3          ; 000050 06 C9
      call   LD_EDGE_1           ; 000052 CD E7 05
      jr     nc,LD_BREAK        ; 000055 30 D5
      ld     a,b             ; 000057 78
      cp     OFF_SYNC_PULSE             ; 000058 FE D4
      jr     nc,LD_SYNC        ; 00005A 30 F4
      call   LD_EDGE_1           ; 00005C CD E7 05
      ret    nc              ; 00005F D0

      ld     a,c             ; 000060 79
      xor    $03             ; 000061 EE 03
      ld     c,a             ; 000063 4F
      ld     h,$00           ; 000064 26 00
      ld     b,FLAG_TIMING_K           ; 000066 06 B0
      jr     LD_MARKER           ; 000068 18 1F

.LD_LOOP
      ex     af,af'          ; 00006A 08
      jr     nz,LD_FLAG        ; 00006B 20 07
      jr     nc,LD_VERIFY        ; 00006D 30 0F
      ld     (ix+$00),l      ; 00006F DD 75 00
      jr     LD_NEXT           ; 000072 18 0F
.LD_FLAG
      rl     c               ; 000074 CB 11
      xor    l               ; 000076 AD
      ret    nz              ; 000077 C0
      ld     a,c             ; 000078 79
      rra                    ; 000079 1F
      ld     c,a             ; 00007A 4F
      inc    de              ; 00007B 13
      jr     LD_DEC           ; 00007C 18 07

.LD_VERIFY
      ld     a,(ix+$00)      ; 00007E DD 7E 00
      xor    l               ; 000081 AD
      ret    nz              ; 000082 C0

.LD_NEXT
      inc    ix              ; 000083 DD 23

.LD_DEC
      dec    de              ; 000085 1B
      ex     af,af'          ; 000086 08
      ld     b,$b2           ; 000087 06 B2

.LD_MARKER
      ld     l,$01           ; 000089 2E 01

.LD_8_BITS
      call   LD_EDGE_2           ; 00008B CD E3 05
      ret    nc              ; 00008E D0
      ld     a,BIT_THRESHOLD           ; 00008F 3E CB
      cp     b               ; 000091 B8
      rl     l               ; 000092 CB 15
      ld     b,DATA_TIMING_K           ; 000094 06 B0
      jp     nc,LD_8_BITS        ; 000096 D2 CA 05
      ld     a,h             ; 000099 7C
      xor    l               ; 00009A AD
      ld     h,a             ; 00009B 67
      ld     a,d             ; 00009C 7A
      or     e               ; 00009D B3
      jr     nz,LD_LOOP        ; 00009E 20 CA
      ld     a,h             ; 0000A0 7C
      cp     $01             ; 0000A1 FE 01
      ret                    ; 0000A3 C9

.LD_EDGE_2
      call   LD_EDGE_1           ; 0000A4 CD E7 05
      ret    nc              ; 0000A7 D0

.LD_EDGE_1
;      nop
;      nop
;      nop
;      nop
;      nop
;      xor a


      ld     a,EDGE_SAMPLING_TIME           ; 0000A8 3E 16

.LD_DELAY
      dec    a               ; 0000AA 3D
      jr     nz,LD_DELAY        ; 0000AB 20 FD
      and    a               ; 0000AD A7
;      xor     a

.LD_SAMPLE
      inc    b               ; 0000AE 04
      ret    z               ; 0000AF C8
      ld     a,$7f           ; 0000B0 3E 7F
      in     a,($fe)         ; 0000B2 DB FE
      rra                    ; 0000B4 1F
      ret    nc              ; 0000B5 D0
      xor    c               ; 0000B6 A9
      and    $20             ; 0000B7 E6 20
      jr     z,LD_SAMPLE     ; 0000B9 28 F3

      ld     a,c             ; 0000BB 79
      cpl                    ; 0000BC 2F
      ld     c,a             ; 0000BD 4F
      and    $07             ; 0000BE E6 07
      or     $08             ; 0000C0 F6 08
      out    ($fe),a         ; 0000C2 D3 FE
      scf                    ; 0000C4 37
      ret                    ; 0000C5 C9
