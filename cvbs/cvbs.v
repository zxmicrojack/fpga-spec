/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */

// As you can see, the visible (“active”) area can display up to 490 x 268 pixels.
//12.05 us =


module cvbs (
    input clk,
    output vout, sync,
    input rstn //
);

pll_cvbs pll_cvbs_inst
(
.inclk0(clk),
.c1(cvbs_clk),               // 13.5Mhz for PAL
.areset(~rstn),
.locked()
);


// reg [2:0] count;
// wire cvbs_clk = count[1];
// always @(posedge clk) begin
//     if (count == 4)
//         count <= 0;
//     else
//         count <= count + 1;
// end

reg [9:0] xpos;
reg [8:0] ypos;
always @(posedge cvbs_clk) begin
    if (xpos == 864) begin
        xpos <= 0;
        if (ypos == 312)
            ypos <= 0;
        else
            ypos <= ypos + 1;
    end else
        xpos <= xpos + 1;
end

wire active = xpos < 490 && ypos < 268;
wire hsync = 701 <= xpos && xpos < 864;
wire vsync = 305 <= ypos && ypos < 312;

assign vout = active && (xpos == 0 || xpos == 489 || ypos == 0 || ypos == 267);
assign sync_ = active || !(hsync || vsync);

endmodule
