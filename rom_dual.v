/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define ROM_SIZE 8192

module rom_single(q, a, d, clk, wren);
   output reg[7:0] q;
   input [7:0] d;
   input [13:0] a;
   input wren;
   input clk;

   reg [7:0] mem [0:`ROM_SIZE-1] /* synthesis ramstyle = "M144K" */;
   initial begin
     $readmemh("bios/bios.hex", mem);
   end

   always @(posedge clk) begin
     if (wren)
         mem[a] <= d;

     q <= mem[a];
   end
endmodule
