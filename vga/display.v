/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
module display(
  input rstn,
  input clk50m,
  input sys_clk,
  input flash_clk,

  // zx spec screen 1 (48k one)
  input[7:0] scr_write,
  input[12:0] scr_write_addr,
  input scr_write_we,

  // zx spec border colour
  input[2:0] border,

  // zx spec screen 2 (128k)
  input[7:0] scr_write2,
  input[12:0] scr_write_addr2,
  input scr_write_we2,

  // zx which screen to show
  input screen_flip,

  // debug / terminal screen
  input screen_clk,
  input[7:0] screen_byte,
  input screen_mode,

  // vga connectors
  output vga_hs,
  output vga_vs,
  output [3:0] vga_r,
  output [3:0] vga_g,
  output [3:0] vga_b
  );

  wire [10 : 0] x_cnt;
  wire [9 : 0]  y_cnt;
  wire hsync_de;
  wire vsync_de;
  wire vga_clk;
  wire [11:0] screen_output;
  wire [11:0] screen_output2;
  wire [11:0] text_output;

  wire [11:0] rgb_out;

  assign rgb_out = screen_mode ? text_output : screen_flip ? screen_output2 : screen_output;

  vga vga(
  			.clk50m(clk50m),
  			.rstn(rstn),
  			.vga_hs(vga_hs),
  			.vga_vs(vga_vs),
  			.vga_r(vga_r),
  			.vga_g(vga_g),
  			.vga_b(vga_b),
        .x_cnt(x_cnt),
        .y_cnt(y_cnt),
        .hsync_de(hsync_de),
        .vsync_de(vsync_de),
        .rgb_out(rgb_out),
        .vga_clk(vga_clk)
  );

  spectrumscreen spectrumscreen(
    .x_cnt(x_cnt),
    .y_cnt(y_cnt),
    .hsync_de(hsync_de),
    .vsync_de(vsync_de),
    .buf_write(scr_write),
    .buf_write_addr(scr_write_addr),
    .buf_we(scr_write_we),
    .buf_write_clk(sys_clk),
    .border(border),
    .flash_clk(flash_clk),
    .vga_clk(vga_clk),
    .screen_output(screen_output)
    );

  spectrumscreen spectrumscreen2(
    .x_cnt(x_cnt),
    .y_cnt(y_cnt),
    .hsync_de(hsync_de),
    .vsync_de(vsync_de),
    .buf_write(scr_write2),
    .buf_write_addr(scr_write_addr2),
    .buf_we(scr_write_we2),
    .buf_write_clk(sys_clk),
    .border(border),
    .flash_clk(flash_clk),
    .vga_clk(vga_clk),
    .screen_output(screen_output2)
    );

  terminalscreen terminalscreen(
    .x_cnt(x_cnt),
    .y_cnt(y_cnt),
    .hsync_de(hsync_de),
    .vsync_de(vsync_de),
    .screen_clk(screen_clk),
    .screen_byte(screen_byte),
    .vga_clk(vga_clk),
    .text_output(text_output),
    );


endmodule
