/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define SCREEN_SIZE 2560

module ram_dual(q, addr_in, addr_out, d, we, clk1, clk2);
   output[7:0] q;
   input [7:0] d;
   input [12:0] addr_in;
   input [12:0] addr_out;
   input we, clk1, clk2;

   reg [12:0] addr_out_reg;
   reg [7:0] q;
   reg [7:0] mem [0:`SCREEN_SIZE-1] /* synthesis ramstyle = "M144K" */;

   initial begin
     integer i;
     for (i=0; i<`SCREEN_SIZE; i=i+1)
       mem[i] <= 32;
       // mem[i] <= 32 + (i % 96);
   end


   always @(posedge clk1) begin
      if (we)
         mem[addr_in] <= d;
   end

   always @(posedge clk2) begin
      addr_out_reg <= addr_out;
      q <= mem[addr_out_reg];
   end

endmodule
