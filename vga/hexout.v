/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
module hexout(
  output reg[7:0] screen_byte,
  output reg  screen_clk,
  input[7:0] data,
  input      data_valid,
  input      clk,
  input      reset //
);

reg[3:0] data_clock_state = 0;

reg [7:0] HEXCHARS [0:15];
initial begin
  $readmemh("hexchars.hex", HEXCHARS);
end

always @(posedge clk) begin
  case (data_clock_state)
    4'b0000: begin
      if (data_valid) data_clock_state <= 4'b0001;
      else if (reset) data_clock_state <= 4'b1000;
    end
    4'b0001: begin screen_byte <= HEXCHARS[data[7:4]]; screen_clk <= 1'b1; data_clock_state <= 4'b0010; end
    4'b0010: begin screen_clk <= 1'b0; data_clock_state <= 4'b0011; end
    4'b0011: begin screen_byte <= HEXCHARS[data[3:0]]; screen_clk <= 1'b1; data_clock_state <= 4'b0100; end
    4'b0100: begin screen_clk <= 1'b0; data_clock_state <= 4'b0101; end
    4'b0101: begin screen_byte <= 7'h20; screen_clk <= 1'b1; data_clock_state <= 4'b0110; end
    4'b0110: begin screen_clk <= 1'b0; data_clock_state <= 4'b0111; end
    4'b0111: if (!data_valid) data_clock_state <= 4'b0000;

    4'b1000: begin screen_byte <= 8'h00; screen_clk <= 1'b1; data_clock_state <= 4'b1001; end
    4'b1001: begin screen_clk <= 1'b0; data_clock_state <= 4'b1010; end
    4'b1010: if (!reset) data_clock_state <= 4'b0000;
    default: data_clock_state <= 4'b0000;
  endcase
end

endmodule
