/* This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo */
`define DEBUG
// `define DEBUG_SINGLESTEP
// `define BREAKPOINT 16'h12F2
// `define BREAKPOINT 16'h1569
// `define BREAKPOINT 16'h1585
// `define BREAKPOINT 16'h0152
// `define BREAKPOINT 16'h11ef
// `define BREAKPOINT 16'h1519
// `define BREAKPOINT 16'h1886 // NMI_READ
// `define BREAKPOINT 16'h1520 // NMI_READ
// `define BREAKPOINT 16'h0556 // load from tape

`define PAGEMEM24_23    2'b11
`define PAGEMEM24_22    3'b110
`define TAP_ADDR_UPPER7 7'b1_1110_00

// `define PAGEMEM24_23    2'b10
// `define PAGEMEM24_22    3'b100

`define MF1
`define MF128
`define MACHINEMENUS
`define OPUS
module main (
            clk,
            rstn,
            led,
            //////// VGA ////////
            vga_hs,
            vga_vs,
            vga_r,
            vga_g,
            vga_b,
            key1,
            //////// ps2 ////////
            kbd_clk,
            kbd_data,
            //////// cvbs ////////
            cvbs_sync,
            cvbs_out,
            //////// audio ////////
            aud_left,
            aud_right,
            ///////// DRAM /////////
            DRAM_ADDR,
            DRAM_BA,
            DRAM_CAS_N,
            DRAM_CKE,
            DRAM_CLK,
            DRAM_CS_N,
            DRAM_DQ,
            DRAM_LDQM,
            DRAM_RAS_N,
            DRAM_UDQM,
            DRAM_WE_N,
            //////// sdcard ////////
            SD_cs,
            SD_clk,
            SD_datain,
            SD_dataout
             );

// misc
input  clk;
input key1;
output led;
input rstn;

// vga
output vga_hs;
output vga_vs;
output [3:0] vga_r;
output [3:0] vga_g;
output [3:0] vga_b;

// PS2
input kbd_clk;
input kbd_data;

// CVBS
output cvbs_sync;
output cvbs_out;

// 1 bit audio dacs
output aud_left;
output aud_right;

// SDRAM
output    [12:0]	DRAM_ADDR;
output    [1:0] 	DRAM_BA;
output            DRAM_CAS_N;
output          	DRAM_CKE;
output            DRAM_CLK;
output        		DRAM_CS_N;
inout     [15:0]	DRAM_DQ;
output            DRAM_LDQM;
output            DRAM_RAS_N;
output            DRAM_UDQM;
output            DRAM_WE_N;

// sdcard
output SD_cs;
output SD_clk;
output SD_datain;
input SD_dataout;

//=======================================================
//  REG/WIRE declarations
//=======================================================

  /////////////////////////////////////////////////////////////////////////////////////////
  // CLOCKS
  wire clk14m, clk24m576;
  clocks clocks(
    .clk50m(clk),
    .clk14m(clk14m),
    .clk24m576(clk24m576)
    );

  reg[31:0] count;
  always @(posedge clk)
    count <= count + 1;

  reg[31:0] count14m;
  always @(posedge clk14m)
    count14m <= count14m + 1;

  reg[31:0] count24m576;
  always @(posedge clk24m576)
    count24m576 <= count24m576 + 1;

  wire sys_clk, ym2149_clk, sys_clk_2, flash_clk, clk_audio, clk_debug;

  // single clock sources
  assign flash_clk = count[24]; // 1.490Hz
  assign led = count[25]; // 0.745MHz
  assign clk_sd = count[5]; // 390.625kHz
  assign sys_clk = count14m[0]; // 7MHz
  assign sys_clk_2 = count14m[1]; // 3.5MHz
  assign ym2149_clk = count14m[1]; // 3.5MHz
  assign clk_audio = count24m576[8]; // 48kHz
  assign clk_debug = count24m576[8]; // 48kHz
  // assign clk_audio = count24m576[9]; // 48kHz
  // assign clk_debug = count24m576[9]; // 48kHz


  /////////////////////////////////////////////////////////////////////////////////////////
  // VGA output
  display display(
    .clk50m(clk),
    .rstn(rstn),
    .sys_clk(sys_clk),
    .flash_clk(flash_clk),

    // zx spec screen 1 (48k one)
    .scr_write(scr_write),
    .scr_write_addr(scr_write_addr),
    .scr_write_we(scr_write_we),

    // zx spec border colour
    .border(border),

    // zx spec screen 2 (128k)
    .scr_write2(scr_write2),
    .scr_write_addr2(scr_write_addr2),
    .scr_write_we2(scr_write_we2),

    // zx which screen to show
    .screen_flip(screen_flip),

    // debug / terminal screen
    .screen_clk(screen_clk),
    .screen_byte(screen_byte),
    .screen_mode(screen_mode),

    // outputs
    .vga_hs(vga_hs),
    .vga_vs(vga_vs),
    .vga_r(vga_r),
    .vga_g(vga_g),
    .vga_b(vga_b)
    );

  // spectrum screen
  reg[7:0] scr_write;
  reg[12:0] scr_write_addr;
  reg scr_write_we;
  reg[2:0] border;

  // spectrum screen2
  reg[7:0] scr_write2;
  reg[12:0] scr_write_addr2;
  reg scr_write_we2;
  wire screen_flip;

  // screen text
  wire screen_clk;
  wire[7:0] screen_byte;
  reg screen_mode;

  /////////////////////////////////////////////////////////////////////////////////////////
  // DEBUG screen output - very early messy code - rationalise and remove
  reg[7:0] data;
  // wire[7:0] data;
  reg data_valid;
  // reg data_valid_pulse;
  reg data_reset;
  hexout hexout(
    .screen_byte(screen_byte),
    .screen_clk(screen_clk),
    // .data(kbd_key),
    // .data_valid(kbd_key_valid),
    .data(data),
    .data_valid(data_valid),
    .clk(clk),
    .reset(data_reset)
  );

  /////////////////////////////////////////////////////////////////////////////////////////
  // PS2 keyboard
  wire [4:0] kvcxzsh, kgfdsa, ktrewq, k54321, k67890, kyuiop, khjklen, kbnmsssp;
  wire [8:0] kspecial;

  kbmain kbmain(
        .kbd_clk(kbd_clk),
        .kbd_data(kbd_data),
        .clk50m(clk),
        .kvcxzsh(kvcxzsh),
        .kgfdsa(kgfdsa),
        .ktrewq(ktrewq),
        .k54321(k54321),
        .k67890(k67890),
        .kyuiop(kyuiop),
        .khjklen(khjklen),
        .kbnmsssp(kbnmsssp),
        .kspecial(kspecial)
        );

  wire kf12, kfpipe, kf11, kf10, kf9, kf8;
  assign kfpipe = kspecial[3];
  assign kf10 = kspecial[2];
  assign kf11 = kspecial[1];
  assign kf12 = kspecial[0];
  assign kf9 = kspecial[4];
  assign kf8 = kspecial[5];
  assign kf7 = kspecial[6];
  assign mf1_button = kspecial[7]; // f6
  assign mf128_button = kspecial[8]; // f5

  /////////////////////////////////////////////////////////////////////////////////////////
  // CVBS - not currently working
  cvbs cvbs (
      .clk(clk),
      .vout(cvbs_out),
      .sync(cvbs_sync),
      .rstn(rstn)
  );

  /////////////////////////////////////////////////////////////////////////////////////////
  // SDRAM

`define NR_NODES  3
  wire [7:0] sdram_dout_m[`NR_NODES];
  reg [24:0] sdram_addr_m[`NR_NODES];
  wire [7:0] sdram_din_m[`NR_NODES];
  wire sdram_we_m[`NR_NODES];
  wire sdram_rd_m[`NR_NODES];

  wire [`NR_NODES*8-1:0] sdram_dout_mx;
  assign sdram_dout_m[0] = sdram_dout_mx[7:0];
  assign sdram_dout_m[1] = sdram_dout_mx[15:8];
  assign sdram_dout_m[2] = sdram_dout_mx[23:16];

  memory memory(
    .clk50m(clk),
    .sdram_dout_m(sdram_dout_mx),
    .sdram_din_m({sdram_din_m[2], sdram_din_m[1], sdram_din_m[0]}),
    .sdram_addr_m({
        5'b11111, disk_addr[19:0],
        `TAP_ADDR_UPPER7, tap_addr[17:0],
        sdram_addr_m[0]
      }),
    .sdram_rd_m({sdram_rd_m[2], sdram_rd_m[1], sdram_rd_m[0]}),
    .sdram_we_m({sdram_we_m[2], sdram_we_m[1], sdram_we_m[0]}),

    // signal pins
    .SDRAM_DQ(DRAM_DQ),
    .SDRAM_A(DRAM_ADDR),
    .SDRAM_DQML(DRAM_LDQM),
    .SDRAM_DQMH(DRAM_UDQM),
    .SDRAM_BA(DRAM_BA),
    .SDRAM_nCS(DRAM_CS_N),
    .SDRAM_nWE(DRAM_WE_N),
    .SDRAM_nRAS(DRAM_RAS_N),
    .SDRAM_nCAS(DRAM_CAS_N),
    .SDRAM_CKE(DRAM_CKE),
    .SDRAM_CLK(DRAM_CLK),
    );

  /////////////////////////////////////////////////////////////////////////////////////////
  // AUDIO

  reg[7:0] ym2149_di;
  wire[7:0] ym2149_do;
  reg ym2149_bdir, ym2149_bc;
  audio audio(
    .aud_left(aud_left),
    .aud_right(aud_right),
    .clk50m(clk),
    .clk_audio(clk_audio),
    .rstn(rstn),

    .ym2149_clk(ym2149_clk),
    .ym2149_di(ym2149_di),
    .ym2149_do(ym2149_do),
    .ym2149_bdir(ym2149_bdir),
    .ym2149_bc(ym2149_bc),

    .ear(ear),
    .RESET(RESET)
  );

  /////////////////////////////////////////////////////////////////////////////////////////
  // SD-CARD
  reg sd_rd = 1'b0;
  reg sd_wr = 1'b0;
  reg[31:0] sd_address = 32'h0000_0000;
  wire sdcard_ready;
  reg sdcard_rfifo_wclk;
  reg sdcard_rfifo_rclk;
  reg sdcard_rfifo_reset;
  wire sdcard_rfifo_empty;
  wire sdcard_rfifo_full;
  wire[7:0] sdcard_rfifo_data;

  sdmain sdmain(
    .SD_cs(SD_cs),
    .SD_clk(SD_clk),
    .SD_datain(SD_datain),
    .SD_dataout(SD_dataout),
    .sd_address(sd_address),
    .sd_rd(sd_rd),
    .sd_wr(sd_wr),
    .rstn(rstn),
    .clk_sd(clk_sd),
    .sdcard_ready(sdcard_ready),
    .sdcard_rfifo_wclk(sdcard_rfifo_wclk),
    .sdcard_rfifo_rclk(sdcard_rfifo_rclk),
    .sdcard_rfifo_reset(sdcard_rfifo_reset),
    .sdcard_rfifo_empty(sdcard_rfifo_empty),
    .sdcard_rfifo_full(sdcard_rfifo_full),
    .sdcard_rfifo_data(sdcard_rfifo_data)
    );

  ///////////////////   OPUS DISCOVERY   ///////////////////
  reg[19:0] disk_size = 0;
  wire[19:0] disk_addr;
  reg[7:0] opd_din;
  wire[7:0] opd_dout;
  reg[1:0] opd_a1_0;
  reg opd_rd;
  reg opd_wr;
  wire opd_drq;

`ifdef OPUS
  wd1770 wd1770(
      .disk_size(disk_size),
      .disk_addr(disk_addr),
      .mem_in(sdram_din_m[2]),
      .mem_out(sdram_dout_m[2]),
      .mem_rd(sdram_rd_m[2]),
      .mem_wr(sdram_we_m[2]),
      .clk(sys_clk_2),

      // interface to cpu
      .din(opd_din),
      .dout(opd_dout),
      .a1_0(opd_a1_0),
      .rd(opd_rd),
      .wr(opd_wr),
      .drq(opd_drq)
    );
`endif

  ///////////////////   MEMORY   ///////////////////
  wire [7:0] rom_read;
  reg [13:0] rom_addr;
  reg [7:0] rom_write;
  reg [7:0] rom_we;

  rom_single rom(
    .q(rom_read),
    .a(rom_addr),
    .d(rom_write),
    .clk(clk),
    .wren(rom_we)
    );

  ///////////////////   CPU   ///////////////////
  wire [15:0] addr;
  reg  [7:0] cpu_din;
  wire  [7:0] cpu_dout;
  wire        nM1;
  wire        nMREQ;
  wire        nIORQ;
  wire        nRD;
  wire        nWR;
  wire        nRFSH;
  wire        nBUSACK;
  reg         INT;
  reg        BUSRQ; // = ~ioctl_download;
  wire        RESET;
  wire         NMI;

  reg _128_mode = 0;
`ifdef DEBUG_SINGLESTEP
  reg sys_running = 1;
`endif
  wire[211:0]	cpu_reg;  // IFF2, IFF1, IM, IY, HL', DE', BC', IX, HL, DE, BC, PC, SP, R, I, F', A', F, A
  reg[211:0]	dir_reg;  // IFF2, IFF1, IM, IY, HL', DE', BC', IX, HL, DE, BC, PC, SP, R, I, F', A', F, A
  reg	dir_set = 1'b0;

  reg[7:0] sdram_din_m0;
  assign sdram_din_m[0] = sdram_din_m0;
  reg sdram_we_m0;
  reg sdram_rd_m0;
  assign sdram_we_m[0] = sdram_we_m0;
  assign sdram_rd_m[0] = sdram_rd_m0;

  T80pa cpu
  (
  	.RESET_n(!RESET),
    // .CLK(count[25]),
`ifdef DEBUG_SINGLESTEP
    .CLK(sys_clk & sys_running),
`else
    .CLK(sys_clk),
`endif
  	.CEN_p(1'b1),
  	.CEN_n(1'b1),
  	.INT_n(!INT),
  	.NMI_n(!NMI),
  	.BUSRQ_n(!BUSRQ),
  	.M1_n(nM1),
  	.MREQ_n(nMREQ),
  	.IORQ_n(nIORQ),
  	.RD_n(nRD),
  	.WR_n(nWR),
  	.RFSH_n(nRFSH),
  	.BUSAK_n(nBUSACK),
  	.A(addr),
  	.DO(cpu_dout),
  	.DI(cpu_din),
  	.REG(cpu_reg),
  	.DIR(dir_reg),
  	.DIRSet(dir_set)
  );

  `define BUFFER_SIZE 6912


  ///////////////////   DEBUG   ///////////////////
// `define CAPTURE 31
`define CAPTURE 239
`ifdef DEBUG

  reg [`CAPTURE:0] captured;
`endif
  ///////////////////   128 specials   ///////////////////
  // 128 memory paging
  reg[2:0] s128_ram_page_ctl;
  reg s128_shadow_screen;
  reg s128_rom_select = 0;
  reg s128_pg_deny;
  // TODO uncomment this - should be flipping screens properly
  // assign screen_flip = _128_mode && s128_shadow_screen;
  // assign screen_flip = 1'b0;

  ///////////////////   TAP INTERFACE   ///////////////////
  reg[17:0] tap_size = 17'h0;
  reg[17:0] tap_tl_pos = 17'h0;
  reg tap_tl_advance = 1'b0;

  reg hyper_loading = 1'b1;
  reg [7:0] tap_block_load_routine [0:72];
  initial begin
    $readmemh("bios/hyperloader.hex", tap_block_load_routine);
  end

  ///////////////////   MEMORY CPU INTERFACE   ///////////////////
  reg[7:0] ram_page = 0;
  reg ear, mic;
  wire ear_in;
`ifdef MF1
  reg mf1_active = 1'b0;
  reg mf1_nmi_pending = 1'b0;
`else
  parameter mf1_active = 1'b0;
`endif

`ifdef MF128
  reg mf128_active = 1'b0;
  reg mf128_nmi_pending = 1'b0;
  reg mf128_enabled = 1'b0;
`else
  parameter mf128_active = 1'b0;
`endif

  reg cold_reset = 1'b1;
  reg[7:0] poreset_delay = 0;

`ifdef MACHINEMENUS
  reg mm_active = 1'b0;
  reg mm_reset = 1'b0;
  reg mm_nmi_pending = 1'b0;
  reg mm_active_tobe = 1'b0;
`else
  parameter mm_active = 1'b0;
`endif
  reg turbo_loading = 1'b1;
  reg opus_active = 1'b0;
  reg opus_active_tobe = 1'b0;
  reg[7:0] mc6821_reg[4];

  assign NMI = mm_nmi_pending || opd_drq || mf1_nmi_pending || mf128_nmi_pending;

`ifdef DEBUG
  reg old_kf11 = 1'b0;
  reg old_m1 = 1'b1;
  reg breakpoint = 1'b0;
`endif

  always @(posedge sys_clk) begin
`ifdef DEBUG
    captured[31:22] <= {mf1_active, mf128_active, turbo_loading, hyper_loading,
      opus_active, mm_active, opus_active_tobe, mm_nmi_pending, mf1_nmi_pending, mf128_nmi_pending};

    if (!nM1 && !nRD) begin
      captured[239:32] <= cpu_reg[207:0];
      captured[15:0] <= addr[15:0];
`ifdef DEBUG_SINGLESTEP
      if (addr[15:0] == `BREAKPOINT) breakpoint <= 1'b1;
`endif
    end
`endif
    if (RESET) begin
      {s128_ram_page_ctl[2:0], s128_shadow_screen, s128_rom_select,
        s128_pg_deny, opus_active_tobe, opus_active, mf1_active,
        mf128_active} <= 10'h000;
      tap_tl_pos <= 0;
      tap_tl_advance <= 0;
`ifdef DEBUG
      captured <= 0;
      breakpoint <= 0;
`endif
    end


`ifdef DEBUG_SINGLESTEP
    if (kf7) breakpoint <= 0;
    if (breakpoint && sys_running && old_m1 && !nM1 && !kf7) sys_running <= 1'b0;
    if (!old_kf11 && kf8 && !sys_running) sys_running <= 1'b1;
    old_kf11 <= kf8;
    old_m1 <= nM1;
`endif

// case (pend_snap)
//   2'b11: begin
//     pend_snap <= 2'b10;
//   end
//   2'b10: begin
//     pend_snap <= 2'b01;
//   end
//   2'b01: begin
//     pend_snap <= 2'b00;
//   end
//   2'b00: begin
//     dir_set <= 1'b0;
//   end
// endcase


`ifdef MF1
    if (mf1_button && !mf1_nmi_pending && !mf1_active) mf1_nmi_pending <= 1'b1;
    if (!mf1_active && !nMREQ && !nM1 && {addr[15:1], 1'b0} == 16'h0066 && mf1_nmi_pending)
      mf1_active <= 1'b1;
`endif

`ifdef MF128
  if (mf128_button && !mf128_nmi_pending && !mf128_active) { mf128_enabled, mf128_nmi_pending } <= 2'b11;
  if (!mf128_active && !nMREQ && !nM1 && {addr[15:1], 1'b0} == 16'h0066 && mf128_nmi_pending)
    mf128_active <= 1'b1;
`endif

`ifdef MACHINEMENUS
  if (mfx_nmi_req && !mm_nmi_pending && !mm_active) mm_nmi_pending <= 1'b1;
  if (!mm_active && !nMREQ && !nM1 && {addr[15:1], 1'b0} == 16'h0066 && mm_nmi_pending)
    mm_active_tobe <= 1'b1;
  if (mm_active && (addr[15:0] == 16'h0100 || addr[15:0] == 16'h0120) && !nRD) begin
    {mm_nmi_pending, mm_active_tobe} <= 2'b00;
  end

  if (cold_reset && !RESET) begin
    mm_active <= 1'b1;
    mm_active_tobe <= 1'b1;
    mm_reset <= 1'b1;
    poreset_delay <= 7'h00;
  end else if (cold_reset && RESET) begin
    if (poreset_delay[7:0] != 8'hff) poreset_delay[7:0] <= poreset_delay[7:0] + 1;
    else {cold_reset, mm_reset} <= {1'b0, 1'b0};
  end

`endif

`ifdef OPUS
  if (!opus_active && !nM1 && (addr[15:0] == 16'h1708 || addr[15:0] == 16'h0008 || addr[15:0] == 16'h0048))
    opus_active_tobe <= 1'b1;

  if (opus_active && !nM1 && addr[15:0] == 16'h1748)
    opus_active_tobe <= 1'b0;
`endif

`define MF1_ROM_MR    13'b10_01_000_100??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:000,MF1MF128MM:100
`define MF1_RAM_MR    13'b10_01_001_100??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:100
`define MF1_RAM_MW    13'b10_10_001_100??? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:001,MF1MF128MM:100S
`define MF128_ROM_MR  13'b10_01_000_010??? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:000,MF1MF128MM:010
`define MF128_RAM_MR  13'b10_01_001_010??? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:001,MF1MF128MM:010
`define MF128_RAM_MW  13'b10_10_001_010??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:010
`define MM_RAM_MW     13'b10_10_001_001??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:001
`define MM_RAM_MR     13'b10_01_001_001??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:001
`define MM_ROM_MR     13'b10_01_000_001??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:000,MF1MF128MM:001

`define IO_WRITE      13'b01_10_???_?????? // nIORC:0,nMREQ:1,nRD:1,nWR:0,a1513:???,MF1MF128MM:???
`define IO_READ       13'b01_01_???_?????? // nIORC:0,nMREQ:1,nRD:0,nWR:1,a1513:???,MF1MF128MM:???

`define M_ROM_R_48    13'b10_01_00?_0000?0 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:00,MF1MF128MM:000
`define M_ROM_R0_128  13'b10_01_00?_000100 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:00,MF1MF128MM:000
`define M_ROM_R1_128  13'b10_01_00?_000110 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:00,MF1MF128MM:000

`define M_RAM_R_A     13'b10_01_01?_?????? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_R_B     13'b10_01_10?_??0??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_R_C_48  13'b10_01_11?_??00?? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_R_C_128 13'b10_01_11?_??01?? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_W_A     13'b10_10_01?_?????? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_W_B     13'b10_10_10?_??0??? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_W_C_48  13'b10_10_11?_??00?? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_W_C_128 13'b10_10_11?_??01?? // nIORC:1,nMREQ:0,nRD:1,nWR:0,a1514:01|10|11,MF1MF128MM:???

`define M_RAM_R_BC_MM 13'b10_01_1??_??1??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???
`define M_RAM_W_BC_MM 13'b10_10_1??_??1??? // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:01|10|11,MF1MF128MM:???

`define OPUS_RAM_MW   13'b10_10_001_000??1 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:001
`define OPUS_RAM_MR   13'b10_01_001_000??1 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:001,MF1MF128MM:001
`define OPUS_ROM_MR   13'b10_01_000_000??1 // nIORC:1,nMREQ:0,nRD:0,nWR:1,a1514:000,MF1MF128MM:001

// page 1 -> 3 used by roms
`define M_ROM_48_POS        {`PAGEMEM24_22, 8'h02, addr[13:0]}        // page 1a
`define M_ROM0_128_POS      {`PAGEMEM24_22, 8'h03, addr[13:0]}        // page 1b
`define M_ROM1_128_POS      {`PAGEMEM24_22, 8'h04, addr[13:0]}        // page 2a
`define MF1_ROM_POS         {`PAGEMEM24_22, 8'h05, 1'b0, addr[12:0]}  // page 2b0
`define MF128_ROM_POS       {`PAGEMEM24_22, 8'h05, 1'b1, addr[12:0]}  // page 2b1
`define OPUS_ROM22_POS      {`PAGEMEM24_22, 8'h06, 1'b0, addr[12:0]}  // page 3b0

`define OPUS_RAM_POS        {`PAGEMEM24_23, 8'h08, 2'b00, addr[12:0]} // page 8a0
`define MF1_RAM_POS         {`PAGEMEM24_23, 8'h08, 2'b01, addr[12:0]} // page 8a1
`define MF128_RAM_POS       {`PAGEMEM24_23, 8'h08, 2'b10, addr[12:0]} // page 8b0
`define MM_RAM_POS          {`PAGEMEM24_23, 8'h08, 2'b11, addr[12:0]} // page 8b1

`define M_RAM_A_POS         {`PAGEMEM24_23, 8'h09, 1'b0, addr[13:0]}  // page 9a
`define M_RAM_B_POS         {`PAGEMEM24_23, 8'h00, 1'b0, addr[13:0]}  // page 0a
`define M_RAM_C_48_POS      {`PAGEMEM24_23, 8'h00, 1'b1, addr[13:0]}  // page 0b
`define M_RAM_C_128_PG0_POS {`PAGEMEM24_23, 8'h00, 1'b1, addr[13:0]}  // page 0b
`define M_RAM_C_128_PG1_POS {`PAGEMEM24_23, 8'h04, 1'b0, addr[13:0]}  // page 4a
`define M_RAM_C_128_PG2_POS {`PAGEMEM24_23, 8'h00, 1'b0, addr[13:0]}  // page 0a
`define M_RAM_C_128_PG3_POS {`PAGEMEM24_23, 8'h04, 1'b1, addr[13:0]}  // page 4b
`define M_RAM_C_128_PG4_POS {`PAGEMEM24_23, 8'h05, 1'b0, addr[13:0]}  // page 5a
`define M_RAM_C_128_PG5_POS {`PAGEMEM24_23, 8'h09, 1'b0, addr[13:0]}  // page 9a
`define M_RAM_C_128_PG6_POS {`PAGEMEM24_23, 8'h05, 1'b1, addr[13:0]}  // page 5b
`define M_RAM_C_128_PG7_POS {`PAGEMEM24_23, 8'h06, 1'b0, addr[13:0]}  // page 6a

`define M_RAM_BC_MM_POS     {`PAGEMEM24_23, ram_page[7:0], addr[14:0]}
// TODO: unused at present

    casez ({nIORQ, nMREQ, nRD, nWR, addr[15:13], mf1_active, mf128_active, mm_active, _128_mode, s128_rom_select, opus_active})
`ifdef MF1
      `MF1_ROM_MR: begin // mf1 rom read
        sdram_addr_m[0][24:0] <= `MF1_ROM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `MF1_RAM_MR: begin // mf1 rom read
        sdram_addr_m[0][24:0] <= `MF1_RAM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `MF1_RAM_MW: begin // mf1 ram write
        sdram_addr_m[0][24:0] <= `MF1_RAM_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end
`endif

`ifdef MF128
      `MF128_ROM_MR: begin // mf128 rom read
        sdram_addr_m[0][24:0] <= `MF128_ROM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `MF128_RAM_MR: begin // mf128 rom read
        sdram_addr_m[0][24:0] <= `MF128_RAM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `MF128_RAM_MW: begin // mf128 ram write
        sdram_addr_m[0][24:0] <= `MF128_RAM_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end
`endif

`ifdef MACHINEMENUS
      `MM_ROM_MR: begin // mf1 rom read
        rom_addr[13:0] <= addr[13:0];
        cpu_din[7:0] <= rom_read[7:0];
      end

      `MM_RAM_MR: begin // mf1 rom read
        sdram_addr_m[0][24:0] <= `MM_RAM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `MM_RAM_MW: begin // mf1 ram write
        sdram_addr_m[0][24:0] <= `MM_RAM_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end
`endif

`ifdef OPUS
      `OPUS_RAM_MW: begin // opus discovery ram
        if (addr[15:2] == 16'hc00) begin
          mc6821_reg[addr[1:0]] <= cpu_dout[7:0];
        end else if (addr[15:2] == 16'ha00) begin
          opd_a1_0[1:0] <= addr[1:0];
          opd_din[7:0] <= cpu_dout[7:0];
          opd_wr <= 1'b1;
        end else begin
          sdram_addr_m[0][24:0] <= `OPUS_RAM_POS;
          sdram_we_m0 <= 1'b1;
          sdram_din_m0[7:0] <= cpu_dout[7:0];
        end
      end

      `OPUS_RAM_MR: begin // opus discovery ram
        if (addr[15:2] == 16'hc00) begin
          cpu_din[7:0] <= mc6821_reg[addr[1:0]];
        end else if (addr[15:2] == 16'ha00) begin
          opd_a1_0[1:0] <= addr[1:0];
          cpu_din[7:0] <= opd_dout[7:0];
          opd_rd <= 1'b1;
        end else begin
          sdram_addr_m[0][24:0] <= `OPUS_RAM_POS;
          sdram_rd_m0 <= 1'b1;
          cpu_din[7:0] <= sdram_dout_m[0][7:0];
        end
      end
      `OPUS_ROM_MR: begin // opus discovery rom v 22
        sdram_addr_m[0][24:0] <= `OPUS_ROM22_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end
`endif
      `M_ROM_R_48: begin // rom read
        // read from 1 10rr rrrr rraa aaaa aaaa aaaa
        if (hyper_loading && addr[13:0] >= 13'h0556 && addr[13:0] < 13'h059e)
          cpu_din[7:0] <= tap_block_load_routine[addr[13:0] - 13'h0556];
        else if (turbo_loading && addr[13:0] == 13'h05cf)
          cpu_din[7:0] <= 8'hb9;
        else if (turbo_loading && addr[13:0] == 13'h05e8)
          cpu_din[7:0] <= 8'h02;
        else begin
          sdram_addr_m[0][24:0] <= `M_ROM_48_POS;
          sdram_rd_m0 <= 1'b1;
          cpu_din[7:0] <= sdram_dout_m[0][7:0];
        end
      end

      `M_ROM_R0_128: begin // rom read
        // read from 1 10rr rrrr rraa aaaa aaaa aaaa
        sdram_addr_m[0][24:0] <= `M_ROM0_128_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_ROM_R1_128: begin // rom read
        // read from 1 10rr rrrr rraa aaaa aaaa aaaa
        if (hyper_loading && addr[13:0] >= 13'h0556 && addr[13:0] < 13'h059e)
          cpu_din[7:0] <= tap_block_load_routine[addr[13:0] - 13'h0556];
        else if (turbo_loading && addr[13:0] == 13'h05cf)
          cpu_din[7:0] <= 8'hb9;
        else if (turbo_loading && addr[13:0] == 13'h05e8)
          cpu_din[7:0] <= 8'h02;
        else begin
          sdram_addr_m[0][24:0] <= `M_ROM1_128_POS;
          sdram_rd_m0 <= 1'b1;
          cpu_din[7:0] <= sdram_dout_m[0][7:0];
        end
      end


      `M_RAM_R_A: begin // ram read
        sdram_addr_m[0][24:0] <= `M_RAM_A_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_RAM_W_A: begin // ram write
        sdram_addr_m[0][24:0] <= `M_RAM_A_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];

        // shadow write to screen memory
        if (addr[13:0] < `BUFFER_SIZE) begin
          scr_write[7:0] <= cpu_dout[7:0];
          scr_write_addr[12:0] <= addr[12:0];
          scr_write_we <= 1'b1;
        end
      end

      `M_RAM_R_B: begin // ram read
        sdram_addr_m[0][24:0] <= `M_RAM_B_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_RAM_W_B: begin // ram write
        sdram_addr_m[0][24:0] <= `M_RAM_B_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end

      `M_RAM_R_C_48: begin // ram read
        sdram_addr_m[0][24:0] <= `M_RAM_C_48_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_RAM_R_C_128: begin // ram read
        case (s128_ram_page_ctl[2:0])
          0: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG0_POS;
          1: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG1_POS;
          2: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG2_POS; // 0x8000 duplicate
          3: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG3_POS;
          4: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG4_POS;
          5: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG5_POS; // screen 0x4000
          6: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG6_POS;
          7: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG7_POS;
        endcase
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_RAM_W_C_128: begin // ram write
        case (s128_ram_page_ctl[2:0])
          0: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG0_POS;
          1: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG1_POS;
          2: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG2_POS; // 0x8000 duplicate
          3: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG3_POS;
          4: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG4_POS;
          5: begin
            sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG5_POS; // screen 0x4000
            // shadow write to screen memory
            if (addr[13:0] < `BUFFER_SIZE) begin
              scr_write[7:0] <= cpu_dout[7:0];
              scr_write_addr[12:0] <= addr[12:0];
              scr_write_we <= 1'b1;
            end
          end

          6: sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG6_POS;
          7: begin
            sdram_addr_m[0][24:0] <= `M_RAM_C_128_PG7_POS; // screen 2
            // shadow write to screen memory
            if (addr[13:0] < `BUFFER_SIZE) begin
              scr_write2[7:0] <= cpu_dout[7:0];
              scr_write_addr2[12:0] <= addr[12:0];
              scr_write_we2 <= 1'b1;
            end
          end
        endcase
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end

      `M_RAM_W_C_48: begin // ram write
        sdram_addr_m[0][24:0] <= `M_RAM_C_48_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end

      `M_RAM_R_BC_MM: begin // ram read
        sdram_addr_m[0][24:0] <= `M_RAM_BC_MM_POS;
        sdram_rd_m0 <= 1'b1;
        cpu_din[7:0] <= sdram_dout_m[0][7:0];
      end

      `M_RAM_W_BC_MM: begin // ram write
        sdram_addr_m[0][24:0] <= `M_RAM_BC_MM_POS;
        sdram_we_m0 <= 1'b1;
        sdram_din_m0[7:0] <= cpu_dout[7:0];
      end

      // read from port
      `IO_READ: begin
        // IN:    Reads keys (bit 0 to bit 4 inclusive)
        //
        //      0xfdfe  A, S, D, F, G                0xdffe  P, O, I, U, Y
        //      0xfefe  SHIFT, Z, X, C, V            0xeffe  0, 9, 8, 7, 6
        //      0xfbfe  Q, W, E, R, T                0xbffe  ENTER, L, K, J, H
        //      0xf7fe  1, 2, 3, 4, 5                0x7ffe  SPACE, SYM SHFT, M, N, B
        casez (addr[15:0])
          // keyboard routines
          16'h00fe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, kvcxzsh & kgfdsa & ktrewq & k54321 & k67890 & kyuiop & khjklen & kbnmsssp};
          16'hfefe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, kvcxzsh};
          16'hfdfe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, kgfdsa};
          16'hfbfe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, ktrewq};
          16'hf7fe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, k54321};
          16'heffe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, k67890};
          16'hdffe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, kyuiop};
          16'hbffe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, khjklen};
          16'h7ffe: cpu_din[7:0] <= {1'b1, ear_in, 1'b1, kbnmsssp};

          // SD card interfacing
          16'h00ff: if (mm_active) cpu_din[7:0] <= {sdcard_rfifo_empty, sdcard_rfifo_full, sdcard_ready, 5'h0};
          16'h01ff: if (mm_active) begin
            sdcard_rfifo_rclk <= 1;
            cpu_din[7:0] <= sdcard_rfifo_data;
          end

          // rom/ram paging
          16'h4ff: if (mm_active) cpu_din[7:0] <= ram_page;

          // 128 mode reg
          16'h7ffd: cpu_din[7:0] <= {2'b00, s128_pg_deny, s128_rom_select, s128_shadow_screen, s128_ram_page_ctl[2:0]};

`ifdef MF1
          16'h??9f: begin // mf1 in
            cpu_din[7:0] <= 8'h00;
            mf1_active <= 1;
          end
          16'h??1f: begin // mf1 out
            cpu_din[7:0] <= 8'h00;
            mf1_active <= 0;
          end
`endif

`ifdef MF128
          16'h??bf: begin // mf128 in
            cpu_din[7:0] <= 8'h00;
            mf128_active <= mf128_enabled;
          end
          16'h??3f: begin // mf128 out
            cpu_din[7:0] <= 8'h00;
            mf128_active <= 0;
          end
`endif

          // ym2149
          // BDIR  BC  MODE
          //   0   0   inactive
          //   0   1   read value
          //   1   0   write value
          //   1   1   set address
          16'hfffd: begin
            cpu_din[7:0] <= ym2149_do[7:0];

            ym2149_bdir <= 1'b0;
            ym2149_bc <= 1'b1;
          end

          16'h0dff: if (hyper_loading) begin
            sdram_addr_m[0][24:0] <= {`TAP_ADDR_UPPER7, tap_tl_pos};
            sdram_rd_m0 <= 1'b1;
            cpu_din[7:0] <= sdram_dout_m[0][7:0];
            tap_tl_advance <= 1'b1;
          end


          default: cpu_din[7:0] <= 8'b11111111;
        endcase
      end

      // write to port
      `IO_WRITE: begin
        if (!addr[0]) begin
          border[2:0] <= cpu_dout[2:0];
          ear <= cpu_dout[4];
          mic <= cpu_dout[3];
        end else if (addr[15:0] == 16'h00ff && mm_active) begin
          sd_rd <= cpu_dout[0];

          if (cpu_dout[1]) begin // reset fifo
            sdcard_rfifo_reset <= 1;
            sdcard_rfifo_rclk <= 1;
            sdcard_rfifo_wclk <= 1;
          end

          // page out menus - TODO remove it
          if (cpu_dout[2]) { mm_nmi_pending, mm_active_tobe } <= 2'b00;

        end else if (addr[15:0] == 16'h01ff) begin
          if (mm_active) sd_address[31:24] <= cpu_dout;
        end else if (addr[15:0] == 16'h02ff) begin
          if (mm_active) sd_address[23:16] <= cpu_dout;
        end else if (addr[15:0] == 16'h03ff) begin
          if (mm_active) sd_address[15:8] <= cpu_dout;
        end else if (addr[15:0] == 16'h04ff) begin
          if (mm_active) ram_page <= cpu_dout;
        end else if (addr[15:0] == 16'h06ff) begin
          if (mm_active) tap_size[17:16] <= cpu_dout[1:0];
        end else if (addr[15:0] == 16'h07ff) begin
          if (mm_active) tap_size[15:8] <= cpu_dout[7:0];
        end else if (addr[15:0] == 16'h08ff) begin
          if (mm_active) tap_size[7:0] <= cpu_dout[7:0];
        end else if (addr[15:0] == 16'h09ff) begin
          if (mm_active) begin
            _128_mode <= cpu_dout[0];
            turbo_loading <= cpu_dout[1];
            hyper_loading <= cpu_dout[2];
            cold_reset <= cpu_dout[7];
          end
        end else if (addr[15:0] == 16'h0aff) begin
          if (mm_active) disk_size[19:16] <= cpu_dout[3:0];
        end else if (addr[15:0] == 16'h0bff) begin
          if (mm_active) disk_size[15:8] <= cpu_dout[7:0];
        end else if (addr[15:0] == 16'h0cff) begin
          if (mm_active) disk_size[7:0] <= cpu_dout[7:0];
        end else if (addr[15:0] == 16'h0dff && hyper_loading) begin
          tap_tl_pos <= 0;
          tap_tl_advance <= 0;
        end else if (addr[15:0] == 16'h7ffd && !s128_pg_deny && _128_mode) begin // 128 mode reg
          {s128_pg_deny, s128_rom_select, s128_shadow_screen, s128_ram_page_ctl} <= cpu_dout[5:0];
`ifdef MF1
        end else if (addr[7:0] == 16'h001f) begin
          mf1_nmi_pending <= 1'b0;
`endif
`ifdef MF128
        end else if (addr[7:0] == 16'h003f) begin
          {mf128_enabled, mf128_nmi_pending} <= 2'b00;
        end else if (addr[7:0] == 16'h00bf) begin
          {mf128_enabled, mf128_nmi_pending} <= 2'b10;
`endif

        // ym2149
        // BDIR  BC  MODE
        //   0   0   inactive
        //   0   1   read value
        //   1   0   write value
        //   1   1   set address
        end else if (addr[15:0] == 16'hfffd) begin
          // Select a register 0-14
          ym2149_di[7:0] <= cpu_dout[7:0];
          ym2149_bdir <= 1'b1;
          ym2149_bc <= 1'b1;

        end else if (addr[15:0] == 16'hbffd) begin
          // Write to the selected register
          ym2149_di[7:0] <= cpu_dout[7:0];
          ym2149_bdir <= 1'b1;
          ym2149_bc <= 1'b0;
        end
      end

      default: begin
        sdcard_rfifo_wclk <= 0;
        sdcard_rfifo_rclk <= 0;
        sdcard_rfifo_reset <= 0;

        sdram_we_m0 <= 1'b0;
        sdram_rd_m0 <= 1'b0;

        scr_write_we <= 1'b0;
        scr_write_we2 <= 1'b0;

        ym2149_bdir <= 1'b0;
        ym2149_bc <= 1'b0;

        opd_rd <= 1'b0;
        opd_wr <= 1'b0;
        opus_active <= opus_active_tobe;
        mm_active <= mm_active_tobe;

        if (tap_tl_advance) begin
          if (tap_tl_pos != tap_size) tap_tl_pos <= tap_tl_pos + 1;
          tap_tl_advance <= 1'b0;
        end
      end
    endcase
  end

  ///////////////////   Generate 50Hz Interrupts   ///////////////////
  reg[17:0] INT_timer = 0;
  `define INT_PERIOD  140000
  `define INT_ONPERIOD   128
  always @(posedge sys_clk) begin
    INT_timer <= INT_timer + 1;

    // if (INT_timer == (`INT_PERIOD-`INT_ONPERIOD))
    //   INT <= 1;
    // if (!nMREQ && !nIORQ || INT_timer == 10000) INT <= 0;
    if (INT_timer == `INT_PERIOD) begin
      INT <= 1'b1;
      INT_timer <= 0;
    end else if (INT_timer == `INT_ONPERIOD)
      INT <= 1'b0;
  end

  ///////////////////   TAP loader   ///////////////////
  wire[17:0] tap_addr;
  wire tap_clk;

  // TODO: fix this - it's horrible
  reg sdram_rd_m1;
  assign sdram_rd_m[1] = sdram_rd_m1;
  always @(posedge sys_clk) begin
    sdram_rd_m1 <= tap_clk;
  end

  reg tap_play = 0;
  taploader taploader(
    .tap_size(tap_size),
    .tap_addr(tap_addr),
    .tap_data(sdram_dout_m[1]),
    .tap_clk(tap_clk),
    .ear_in(ear_in),
    .tap_play(tap_play),
    .clk(clk_audio),
    .turbo_loading(turbo_loading)
    );

  ///////////////////   MISC keyboard controls   ///////////////////
  assign RESET = kf12 || mm_reset;
  assign mfx_nmi_req = kf9;

  always @(posedge kfpipe)
    screen_mode <= !screen_mode;

  always @(posedge kf11)
    tap_play <= !tap_play;

`ifdef DEBUG
  reg[2:0] pcout = 0;

  reg[`CAPTURE:0] captures;
  reg[7:0] capture_size = 0;

  always @(posedge clk_debug) begin
    case (pcout)
      0: if (kf10) begin
        pcout <= 1;
        captures[`CAPTURE:0] <= captured[`CAPTURE:0];
        capture_size <= 0;
        data_reset <= 1;
      end
      1: begin
        data_reset <= 0;
        data_valid <= 1'b0;
        data[7:0] <= captures[`CAPTURE:`CAPTURE-7];
        capture_size[7:0] <= capture_size + 8;
        pcout[2:0] <= 3'h2;
      end
      2: begin
        data_valid <= 1'b1;
        captures[`CAPTURE : 0] <= { captures[`CAPTURE-8:0], 8'h00 };
        if (capture_size != (`CAPTURE+1))
          pcout[2:0] <= 3'h1;
        else
          pcout[2:0] <= 3'h3;
      end
      3: begin
        data_valid <= 1'b0;
        data_reset <= 1'b0;
        pcout <= 3'h4;
      end
      4: if (!kf10) pcout <= 3'b000;
      default: pcout <= 0;
    endcase
  end
`endif

endmodule
