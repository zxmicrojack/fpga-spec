FPGA-SPEC by ZXMicroJack
========================

Tools:
- Quartus 19
- z88dk

Building
========

BIOS
----
This is the bootstrap which loads the ROMs from the SDCARD.  Uses Z88DK to build.
Also there's a hyperloader which simply pulls data from the "tape" directly into
memory via a IO read.  This routine overlays the spectrums ROM at 0x556.  The FPGA
code detects the call then maps in the hyperloader.

This needs to be built in order to boot.

Tools
-----
These are a number of tools including the file system for the SDcard, which is a
very simple version for now.  I plan to put a FAT32 system later on.  Uses gcc to
build.  There's also an emulator for testing some of the bios.

Data
----
Files required to boot bios

1_rom0.bin - 48k spectrum rom (16k)
2_rom128.bin - 128k toastrack rom (32k)
3_mf1.bin - multiface 1 rom (8k)
4_mf128.bin - multiface 128 rom (8k)
5_opus22.bin - opus discovery rom v2.2 (8k)
6_quickdos.bin - opus discovery rom quickdos (8k) - not used at present

For the rest just copy TAP and OPD files in here (only these files supported as yet)

When all is set, make an SDcard:
 - ./mkcard.sh

Then dump it to the 2GB (max atm) sdcard:
 - sudo dd if=sdcard.xxx of=/dev/mmcblk0 (or whatever)

Remember to power down the board before inserting SDcard - no insert detection is done
