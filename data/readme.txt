Files required to boot bios

1_rom0.bin - 48k spectrum rom (16k)
2_rom128.bin - 128k toastrack rom (32k)
3_mf1.bin - multiface 1 rom (8k)
4_mf128.bin - multiface 128 rom (8k)
5_opus22.bin - opus discovery rom v2.2 (8k)
6_quickdos.bin - opus discovery rom quickdos (8k) - not used at present

For the rest just copy TAP and OPD files in here (only these files supported as yet)

When all is set, make an SDcard:
 - ./mkcard.sh

Then dump it to the 2GB (max atm) sdcard:
 - sudo dd if=sdcard.xxx of=/dev/mmcblk0 (or whatever)

Remember to power down the board before inserting SDcard - no insert detection is done
