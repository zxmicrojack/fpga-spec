; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
.memdump
  push hl
  call memdump_page
  call waitkey
  push af
  call waitreleasekey
  pop af
  pop hl

  cp '6'
  jr  nz,memdump_casekeynext1
  inc h
  jr memdump

.memdump_casekeynext1
  cp '7'
  jr  nz,memdump_casekeynext2
  dec h
  jr memdump

.memdump_casekeynext2
  cp '5'
  jr  nz,memdump_casekeynext3
  ld a,h
  sub a,$10
  ld h,a
  jr memdump

.memdump_casekeynext3
  cp '8'
  jr  nz,memdump_casekeynext4
  ld a,h
  add a,$10
  ld h,a
  jr memdump

.memdump_casekeynext4
  cp '1'
  jr  nz,memdump_casekeynext5
  ld bc,MEM_PAGING_REG
  in a,(c)
  dec a
  out (c),a
  jr memdump

.memdump_casekeynext5
  cp '2'
  jr  nz,memdump_casekeynextX
  ld bc,MEM_PAGING_REG
  in a,(c)
  inc a
  out (c),a
  jr memdump

.memdump_casekeynextX
  cp 'x'
  jr  nz,memdump
  ret

.memdump_page
  push hl
  ld d,1 ; blue background
  ld e,7 ; white foreground
  call clearscreen

  ld hl,SCREEN_ATTR_POS
  ld b,4
.memdump_addrfield_loop
  ld (hl),$39
  inc hl
  djnz memdump_addrfield_loop
  inc hl

  ld b,6
.memdump_datafield_loop
  ld (hl),$0e
  inc hl
  ld (hl),$0e
  inc hl
  inc hl
  inc hl
  djnz memdump_datafield_loop

  ld hl,SCREEN_ATTR_POS
  ld de,SCREEN_ATTR_POS+32
  ld bc,21*32
  ldir

  pop hl

  xor a

  ld b,22
.memdump_loopline
  push bc

  ld a,22
  sub b
  ld d,a
  xor a
  ld e,a
  call putat

  ld a,h
  call puthex
  ld a,l
  call puthex

  ld a,':'
  push hl
  call putchar
  pop hl

  ld b,12
.memdump_loopbytes
  ld a,(hl)
  call puthex
;  ld a,' '
;  call putchar
  inc hl
  djnz memdump_loopbytes

  pop bc
  djnz memdump_loopline

  ld de,$1700
  call putat
  ld hl,memdump_msg1
  call putstring
  ld bc,MEM_PAGING_REG
  in a,(c)
  call puthex

  ret

.memdump_msg1
  defb "pg:",$00
