; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
extern putchar
extern putat
extern putstring
extern clearscreen

;#define CHARSET     15616
#define CHARSET       font

.specscreen
  ld d,0
  ld e,7
  call clearscreen

  ld b,1
  ld d,19
  ld e,31
.specscreen_triangleloop
  push de
  push bc

  call putat

.specscreen_triangleloop1

  push bc
  ld b,8
  push de
  ld a,$01
.specscreen_triangleloop2
  ld (de),a
  sll a
  inc d
  djnz specscreen_triangleloop2
  pop de
  inc de
  pop bc

  ; ld a,'0'
  ; call putchar
  djnz specscreen_triangleloop1

  pop bc
  pop de
  inc d
  dec e
  inc b

  ld a,b
  cp 6
  jr nz,specscreen_triangleloop

  ld hl,SCREEN_ATTR_POS+19*32+31
  ld b,1
  ld ix,specscreen_attr

.specscreen_attrloop1
  push bc

  push ix
  push hl
.specscreen_attrloop2
  ld a,(ix)
  ld (hl),a
  inc ix
  inc hl
  djnz specscreen_attrloop2
  pop hl
  pop ix

  ld bc,31
  add hl,bc
  pop bc
  inc b
  ld a,b
  cp 6
  jr nz,specscreen_attrloop1

  ret

.specscreen_attr
  defb @000010,@010110,@110100,@100001,@001000


;   ld hl,$4000 + $1000 + 2*32 + 31
;   ld b,5
;
; .specscreen_triangleloop
;
;   ld b,1
;
;   push hl
;   push bc
;
; .specscreen_triangleloop2
;   push bc
;   push hl
;
;   ld b,8
;   ld a,$01
; .specscreen_triangleloop1
;   ld (hl),a
;   sll a
;   inc h
;   djnz specscreen_triangleloop1
;   pop hl
;   inc hl
;   djnz specscreen_triangleloop2
;
;   pop hl
;   ld bc,30
;   adc hl,bc
;
;   pop bc
;   inc b

  ; ret




; mods: af
; out: z if symbol shift held
.is_ss
  ld a,$7f
  in a,($fe)
  bit 1,a
  ret

; mods: af
; out: z if caps shift held
.is_cs
  ld a,$fe
  in a,($fe)
  bit 0,a
  ret

; mods: bc, hl
; out: a -> key pressed
.waitkey
  call getkey
  cp $00
  jr  z,waitkey
  ret

; mods: af, bc, hl
.waitreleasekey
  call getkey
  cp $00
  jr  nz,waitreleasekey
  ret

; mods: bc, hl
; out: a -> key pressed
.getkey
  ld bc,$f7fe
  ld hl,kbdrow_f7fe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_1
  jr getkey_decode

.getkey_next_1
  ld bc,$effe
  ld hl,kbdrow_effe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_2
  jr getkey_decode

.getkey_next_2
  ld bc,$bffe
  ld hl,kbdrow_bffe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_3
  jr getkey_decode

.getkey_next_3
  ld bc,$7ffe
  ld hl,kbdrow_7ffe

  in a,(c)
  set 1,a ; ignore symbolshift
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_4
  jr getkey_decode

.getkey_next_4
  ld bc,$fefe
  ld hl,kbdrow_fefe

  in a,(c)
  set 0,a ; ignore capsshift
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_5
  jr getkey_decode

.getkey_next_5
  ld bc,$fdfe
  ld hl,kbdrow_fdfe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_6
  jr getkey_decode

.getkey_next_6
  ld bc,$fbfe
  ld hl,kbdrow_fbfe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_7
  jr getkey_decode

.getkey_next_7
  ld bc,$dffe
  ld hl,kbdrow_dffe

  in a,(c)
  call getkey_scan

  xor a
  cp b
  jr z,getkey_next_8
  jr getkey_decode

.getkey_next_8
  ret

.getkey_decode

  call is_ss
  jr  nz,getkey_decode1
  ld a,b
  add a,5
  ld b,a
  jr getkey_decode2

.getkey_decode1
  call is_cs
  jr  nz,getkey_decode2
  ld a,b
  add a,10
  ld b,a

.getkey_decode2
  ld c,b
  dec c
  xor a
  ld b,a
  add hl,bc
  ld a,(hl)
  ret

.kbdrow_f7fe defb "54321%$#@!",$05,"$#@!"
.kbdrow_effe defb "67890&\'()_",$06,$07,$08,"9",$0a
.kbdrow_bffe defb "hjkl",$0d,"^-+=",$0d,"HJKL",$0d
.kbdrow_7ffe defb "bnm",$00," ","*,.",$00," ","BNM",$00," "
.kbdrow_fefe defb "vcxz",$00,"/?#:",$00,"VCXZ",$00
.kbdrow_fdfe defb "gfdsa","}{\\|~","GFDSA"
.kbdrow_fbfe defb "trewq","><EWQ","TREWQ"
.kbdrow_dffe defb "yuiop","[]I;\"","YUIOP"

.getkey_scan
  ld b,5
.getkey_scan_loop
  sra a
  jr nc,getkey_pressed
  djnz getkey_scan_loop
.getkey_pressed
  ret

.clearscreen
  ld a,d
  out ($fe),a
  sla a
  sla a
  sla a
  or a,e

  ld hl,SCREEN_ATTR_POS
  ld de,22529
  ld bc,767
  ld (hl),a
  ldir

  ld hl,16384
  ld de,16385
  ld bc,6143
  xor a
  ld (hl),a
  ldir

  ret

; in: hl -> pointer to string
; mods: af, hl
; out: de -> new position on screen
.putstring
  ld a,(hl)
  cp 0
  ret z

  push hl
  call putchar
  pop hl
  inc hl
  jr putstring

; print at d=x,e=y
; in: de -> xy
; mods: af
; out: de -> video position
.putat
  push de
  ; calculate screen address
  ld a,d
  sla a
  sla a
  sla a
  sla a
  sla a
  and a,$e0

  ; point de to video memory
  pop de
  or a,e
  ld e,a
  ld a,d
  and a,$18
  ld d,a
  set 6,d

  ret

; print character in A at video position DE
; in: A -> char
; in: DE -> video position
; mods: AF, HL
; out: DE -> next video position
.putchar
  push bc
  ; calculate font position
  sub a,32

  ld h,a
  ld l,a

  sra h
  sra h
  sra h
  sra h
  sra h
  sla l
  sla l
  sla l

  ld bc,CHARSET
  add hl,bc

  ld b,8
  push de
.render
  ld a,(hl)
  ld (de),a
  inc hl
  inc d
  djnz render

  ; increment screen position
  pop de
  inc de

  xor a
  cp e

  jr nz,putchar_exit

  ld a,d
  add a,7
  ld d,a
  xor a
  ld e,a

.putchar_exit
  pop bc
  ret

  .hexchars
    defb "0123456789ABCDEF"

  .puthex
    push hl
    push bc
    push af
    rlca
    rlca
    rlca
    rlca
    and $0f

    ld hl,hexchars
    ld c,a
    ld b,0

    add hl,bc
    ld a,(hl)
    call putchar

    pop af
    push af
    and $0f

    ld hl,hexchars
    ld c,a
    ld b,0

    add hl,bc
    ld a,(hl)
    call putchar

    pop af
    pop bc
    pop hl
    ret

.font
  BINARY "font.dat"
