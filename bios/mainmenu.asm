; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
IFDEF RAMBUILD
  INCLUDE "common.asm"
  org 28672
ENDIF

.mainmenu
.mainmenuloop
  ld hl,mainmenu_data
  ld de,mainmenu_title
  call menu

  ld a,l
  cp 0
  jr z,machinetype_menu
  cp 1
  jr z,loadtape_menu
  cp 2
  jp z,loaddisk_blank
  cp 3
  jp z,loaddisk_menu
  cp 4
  jr z,do_memdump
  cp 5
  call z,memtest
  cp 6
  call z,memtest_all
  cp 7
  ret z
  jr mainmenuloop

.do_memdump
  ld hl,$8000
  call memdump
  jr mainmenu

.machinetype_menu
  ld hl,machinetype_data
  ld de,machinetype_title
  call menu

  ld a,l
  ld bc,MACHINE_TYPE
  out (c),a
  jp perform_reset

  ; do something
  jr mainmenuloop

.loadtape_menu
  ld a,MEMPAGE_DIRSCRATCH
  call pagemem
  ;
  ld a,'t'
  call read_directory_from_sdcard
  ld hl,DIRTEXT_POS
  ld de,selecttape_title
  call menu

  ; handle progress indicator
  ld de,0
  call putat
  ld (videopos),de

  ; multiply by 8 to get file position
  or a ; reset carry flag
  rl l
  rl h
  rl l
  rl h
  rl l
  rl h
  ld bc,DIRINFO_POS
  add hl,bc
  push hl
  pop ix
  ld a,MEMPAGE_TAPE ; page 192 for tape
  ld hl,loading_tape
  call read_file_from_sdcard

  ld a,MEMPAGE_DIRSCRATCH
  call pagemem

  ; inform microcode of tap size
  ld bc,TAPSIZE_17_16
  ld a,(ix+6)
  out (c),a

  inc b
  ld a,(ix+5)
  out (c),a

  inc b
  ld a,(ix+4)
  out (c),a

  ld bc,TAP_HYPERLOAD_RESET
  out (c),a

  jp mainmenuloop

.loaddisk_blank
  ld b,6   ; 6 pages for SSSD
  ld a,MEMPAGE_DISK ; page 224 for disk

.loaddisk_blank1
  push bc
  ld bc,MEM_PAGING_REG
  out (c),a
  inc a
  push af

  ; fill out bank with $e5
  ld hl,$8000
  ld de,$8001
  ld (hl),$e5
  ld bc,$7fff
  ldir

  pop af
  pop bc
  djnz loaddisk_blank1

  ; now load blank cat sector
  ld a,MEMPAGE_DISK ; page 224 for disk
  ld bc,MEM_PAGING_REG
  out (c),a
  ld hl,opusblank
  ld de,$8000
  ld bc,opusblankend-opusblank
  ldir

  jp mainmenuloop

.loaddisk_menu
  ld a,MEMPAGE_DIRSCRATCH
  call pagemem

  ld a,'o'
  call read_directory_from_sdcard
  ld hl,DIRTEXT_POS
  ld de,selectdisk_title
  call menu

  ; handle progress indicator
  ld de,0
  call putat
  ld (videopos),de

  ; multiply by 8 to get file position
  or a ; reset carry flag
  rl l
  rl h
  rl l
  rl h
  rl l
  rl h
  ld bc,DIRINFO_POS
  add hl,bc
  push hl
  pop ix
  ld a,MEMPAGE_DISK ; page 224 for disk
  ld hl,loading_disk
  call read_file_from_sdcard

  ; inform microcode of disk size
  ld a,MEMPAGE_DIRSCRATCH
  call pagemem

  ld bc,DISKSIZE_19_16
  ld a,(ix+6)
  out (c),a

  inc b
  ld a,(ix+5)
  out (c),a

  inc b
  ld a,(ix+4)
  out (c),a

  jp mainmenuloop

.mainmenu_title
  defb "Main Menu", $00, $00

.mainmenu_data
  defb "Machine type", $00
  defb "Load tape", $00
  defb "Load blank disk", $00
  defb "Load disk", $00
  defb "Memory dump", $00
  defb "Memory test", $00
  defb "Memory test all pages", $00
  defb "Exit", $00
  defb $00

.machinetype_title
  defb "Machine type", $00, $00

.machinetype_data
  defb "ZXSpectrum 48k", $00
  defb "ZXSpectrum 128k", $00
  defb "ZXSpectrum 48k TurboLoad", $00
  defb "ZXSpectrum 128k TurboLoad", $00
  defb "ZXSpectrum 48k Hyperload", $00
  defb "ZXSpectrum 128k Hyperload", $00
  defb $00

.selecttape_title
  defb "Select Tape", $00, $00

.selectdisk_title
  defb "Select Disk", $00, $00

.nodata
  defb "Data to be loaded", $00, $00

.loading_tape
  defb "Loading tape into memory...", $00

.loading_disk
  defb "Loading disk into memory...", $00

.opusblank
  defb $18, $05, $28, $12, $40, $42, $03, $7e, $dd, $77, $00, $23, $7e, $dd, $77, $01
  defb $dd, $7e, $02, $e6, $2f, $57, $23, $7e, $e6, $d0, $b2, $dd, $77, $02, $c9, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff, $ff
  defb $40, $e5, $01, $f7, $18, $4e, $0c, $00, $03, $f5, $01, $fe, $01, $27, $01, $00
  defb $01, $07, $01, $01, $01, $f7, $16, $4e, $0c, $00, $03, $f5, $01, $fb, $40, $e5
  defb $40, $e5, $40, $e5, $40, $e5, $01, $f7, $18, $4e, $0c, $00, $03, $f5, $01, $fe
  defb $01, $27, $01, $00, $01, $0e, $01, $01, $01, $f7, $16, $4e, $0c, $00, $03, $f5
  defb $01, $fb, $40, $e5, $40, $e5, $40, $e5, $40, $e5, $01, $f7, $18, $4e, $0c, $00
  defb $03, $f5, $01, $fe, $01, $27, $01, $00, $01, $03, $01, $01, $01, $f7, $16, $4e
  defb $0c, $00, $03, $f5, $01, $fb, $40, $e5, $40, $e5, $40, $e5, $40, $e5, $01, $f7
  defb $18, $4e, $0c, $00, $03, $f5, $01, $fe, $01, $27, $01, $00, $01, $0a, $01, $01
  defb $ff, $00, $00, $00, $06, $00, $62, $6c, $61, $6e, $6b, $20, $20, $20, $20, $20
  defb $ff, $00, $cf, $02, $ff, $ff, $62, $6c, $61, $6e, $6b, $20, $20, $20, $20, $20
.opusblankend


IFDEF RAMBUILD
INCLUDE "menus.asm"
INCLUDE "screen.asm"
INCLUDE "fakesdcard.asm"
INCLUDE "filesystem.asm"
INCLUDE "bootloader.asm"
INCLUDE "memtest.asm"
INCLUDE "memdump.asm"

INCLUDE "fakescratch.asm"
.perform_reset
  ret
ENDIF
