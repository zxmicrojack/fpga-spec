; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
org 28672

INCLUDE "common.asm"

  ld hl,16384
  call memdump
  ret

INCLUDE "menus.asm"
INCLUDE "screen.asm"
INCLUDE "memdump.asm"
