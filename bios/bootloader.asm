; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo

.load_roms_from_sdcard
  ; wait for sdram to settle

  ; ld bc,SD_FIFO_STATE
  ; in a,(c)
  ; bit 4,a
  ; jr z,load_roms_from_sdcard

  ; ld d,1 ; blue background
  ; ld e,7 ; white foreground
  ; call clearscreen
  ; ld d,0
  ; ld e,0
  ; call putat
  ; ld hl,loading_msg
  ; call putstring
  ; ld (videopos),de

  call sdreset
  ; call output_progress

  ; read first block from sdcard
  ld hl,32768
  ld de,0
  call sdread_block
  ; call output_progress

  ; read first block from card into page 1 of ram
  ld ix,32792
  ld a,1
  ld hl,loading_msg
  call read_file_from_sdcard

  ; display boot message and return
  ; ld de,(videopos)
  ; ld hl,booting_msg
  ; call putstring
  ; ld (videopos),de
  ret

.output_progress
  push de
  push bc
  push hl
  ld de,(videopos)
  ld a,$2e
  call putchar
  ld (videopos),de
  pop hl
  pop bc
  pop de
  ret

.loading_msg defb "Loading roms from sdcard ",$00
.booting_msg defb "done",$00
