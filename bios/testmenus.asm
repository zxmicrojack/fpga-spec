; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
INCLUDE "common.asm"
  org 28672

  ld de,256
  ld hl,menutitle

  call progress
  ld b,0
.loop
  push bc
  ld b,0
  .delay
  djnz delay
  pop bc


  call progress_update
  djnz loop
  call waitkey
  call waitreleasekey


  ld hl,menudata
  ld de,menutitle
  ld bc,22
  call dispmenu
  call waitkey
  call waitreleasekey

  ld hl,menudata
  ld de,menutitle
  ld bc,1
  call dispmenu
  call waitkey
  call waitreleasekey

  ld hl,menudata
  ld de,menutitle
  ld bc,0
  call dispmenu
  call waitkey
  call waitreleasekey

  ld hl,menudata
  ld de,menutitle
  ld a,21
  call menupage

  ld hl,menudatashort
  ld de,menutitle
  ld a,5
  call menupage

  ; index beyond end of menu
  ld hl,menudata3
  ld de,menutitle
  ld bc,44
  call dispmenu
  call waitkey
  call waitreleasekey

  ld hl,menudata
  ld de,menutitle
  call menu

  ld hl,menudata2
  ld de,menutitle
  call menu

  ld hl,menudata3
  ld de,menutitle
  call menu

  ld hl,menudatashort
  ld de,menutitle
  call menu

  ret

  .menutitle
    defb "Title of menu", $00, $00

  .menudatashort
    defb "Menu item 1", $00
    defb "Menu item 2", $00
    defb "Menu item 3", $00
    defb "Menu item 4", $00
    defb "Menu item 5", $00
    defb $00


.menudata
  defb "Menu item 1", $00
  defb "Menu item 2", $00
  defb "Menu item 3", $00
  defb "Menu item 4", $00
  defb "Menu item 5", $00
  defb "Menu item 6", $00
  defb "Menu item 7", $00
  defb "Menu item 8", $00
  defb "Menu item 9", $00
  defb "Menu item 10", $00
  defb "Menu item 11", $00
  defb "Menu item 12", $00
  defb "Menu item 13", $00
  defb "Menu item 14", $00
  defb "Menu item 15", $00
  defb "Menu item 16", $00
  defb "Menu item 17", $00
  defb "Menu item 18", $00
  defb "Menu item 19", $00
  defb "Menu item 20", $00
  defb "Menu item 21", $00
  defb "Menu item 22", $00
  defb "Menu item 23", $00
  defb "Menu item 24", $00
  defb "Menu item 25", $00
  defb "Menu item 26", $00
  defb "Menu item 27", $00
  defb "Menu item 28", $00
  defb "Menu item 29", $00
  defb "Menu item 30", $00
  defb $00

.menudata2
  defb "Menu item 1", $00
  defb "Menu item 2", $00
  defb "Menu item 3", $00
  defb "Menu item 4", $00
  defb "Menu item 5", $00
  defb "Menu item 6", $00
  defb "Menu item 7", $00
  defb "Menu item 8", $00
  defb "Menu item 9", $00
  defb "Menu item 10", $00
  defb "Menu item 11", $00
  defb "Menu item 12", $00
  defb "Menu item 13", $00
  defb "Menu item 14", $00
  defb "Menu item 15", $00
  defb "Menu item 16", $00
  defb "Menu item 17", $00
  defb "Menu item 18", $00
  defb "Menu item 19", $00
  defb "Menu item 20", $00
  defb "Menu item 21", $00
  defb "Menu item 22", $00
  defb "Menu item 23", $00
  defb "Menu item 24", $00
  defb "Menu item 25", $00
  defb "Menu item 26", $00
  defb "Menu item 27", $00
  defb "Menu item 28", $00
  defb "Menu item 29", $00
  defb "Menu item 30", $00
  defb "Menu item 31", $00
  defb "Menu item 32", $00
  defb "Menu item 33", $00
  defb $00

.menudata3
  defb "Menu item 1", $00
  defb "Menu item 2", $00
  defb "Menu item 3", $00
  defb "Menu item 4", $00
  defb "Menu item 5", $00
  defb "Menu item 6", $00
  defb "Menu item 7", $00
  defb "Menu item 8", $00
  defb "Menu item 9", $00
  defb "Menu item 10", $00
  defb "Menu item 11", $00
  defb "Menu item 12", $00
  defb "Menu item 13", $00
  defb "Menu item 14", $00
  defb "Menu item 15", $00
  defb "Menu item 16", $00
  defb "Menu item 17", $00
  defb "Menu item 18", $00
  defb "Menu item 19", $00
  defb "Menu item 20", $00
  defb "Menu item 21", $00
  defb "Menu item 22", $00
  defb "Menu item 23", $00
  defb "Menu item 24", $00
  defb "Menu item 25", $00
  defb "Menu item 26", $00
  defb "Menu item 27", $00
  defb "Menu item 28", $00
  defb "Menu item 29", $00
  defb "Menu item 30", $00
  defb "Menu item 31", $00
  defb "Menu item 32", $00
  defb "Menu item 33", $00
  defb "Menu item 34", $00
  defb "Menu item 35", $00
  defb "Menu item 36", $00
  defb "Menu item 37", $00
  defb "Menu item 38", $00
  defb "Menu item 39", $00
  defb "Menu item 40", $00
  defb "Menu item 41", $00
  defb "Menu item 42", $00
  defb "Menu item 43", $00
  defb "Menu item 44", $00
  defb $00

INCLUDE "menus.asm"
INCLUDE "screen.asm"
INCLUDE "fakescratch.asm"
