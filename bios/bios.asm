; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
INCLUDE "common.asm"
;#define videopos    $5b00
#define videopos    $2000+$1b00
#define dirtext     $2000+$1b00+2
#define dirinfo     $2000+$1b00+4
#define stackptr    $2000+$1b00+6
#define progress_seg $2000+$1b00+8
#define progress_count $2000+$1b00+10
#define progress_pos $2000+$1b00+12


#define mmstackptr  $3fff
#define screensave  $2000
#define fsscratch_area $8000

  org 0
  di
  jp bootstrap

.int_entrypoint_ff
  defs $38-int_entrypoint_ff,$ff
.int_entrypoint
  reti

.nmi_entrypoint_ff
  defs $66-nmi_entrypoint_ff,$ff

.nmi_entrypoint
  di
  ld (stackptr),sp
  ld sp,mmstackptr

  push af
  push bc
  push de
  push hl
  push ix

  ; save screen
  ld hl,16384
  ld de,screensave
  ld bc,6912
  ldir

  call machinemenu

  ; restore screen
  ld hl,screensave
  ld de,16384
  ld bc,6912
  ldir

  pop ix
  pop hl
  pop de
  pop bc
  pop af

  ld sp,(stackptr)
  jp nmi_return

;  ei
.nmi_entrypoint_nops
  defs $0ff-nmi_entrypoint_nops,$00
.nmi_return
  ei
  retn

.nmi_entrypoint_nops_rst
  defs $120-nmi_entrypoint_nops_rst,$00

.perform_reset
  rst 0

.bootstrap
  ld sp,mmstackptr
  call load_roms_from_sdcard

  ld bc,MEM_PAGING_REG
  ld a,$08
  out (c),a

  ; fill out bank 8 with zeros
  xor a
  ld hl,$8000
  ld de,$8001
  ld (hl),a
  ld bc,$7fff
  ldir


  jp perform_reset ; page out and reset
  ; jp $120 ; page out and reset

  ; copy page switching to ram and run

; .perform_reset
;   ld hl,reset
;   ld de,16384
;   ld bc,resetend-reset
;   ldir
;   jp 16384
;
; .reset
;   ; page out machinemenu and reset spectrum
;   di
;   ld bc,$00ff
;   ld a,$04
;   out (c),a
;   rst 0
;   ; shouldn't get here
; .resetend

.machinemenu
  ;di
  ;ld sp,28671

  call mainmenu
  ;xor a
  ;in a,($1f)
  ret

INCLUDE "bootloader.asm"
INCLUDE "screen.asm"
INCLUDE "sdcard.asm"
INCLUDE "filesystem.asm"
INCLUDE "menus.asm"
INCLUDE "mainmenu.asm"
INCLUDE "memdump.asm"
INCLUDE "memtest.asm"
