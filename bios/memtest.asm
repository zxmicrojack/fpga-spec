; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
.memtest_all
  xor a
.memtest_all_loop
  ld bc,MEM_PAGING_REG
  out (c),a
  push af

  ld d,1 ; blue background
  ld e,7 ; white foreground
  call clearscreen

  ld de,0
  call putat

  ld hl,memtest_page_string
  call putstring

  pop af
  push af
  call puthex

  call memtest_page
  jr c,memtest_all_exit
  jr memtest_all_next

.memtest_all_exit
  ld d,1 ; blue background
  ld e,7 ; white foreground
  call clearscreen
  ld de,$00
  call putat
  ld hl,compare_all_fail_string
  call putstring

  pop af
  call puthex

  call waitkey
  call waitreleasekey
  ret

.memtest_all_next
  pop af
  inc a
  cp 0
  jr z,memtest_all_finished
  jr memtest_all_loop

.memtest_all_finished
  ld d,1 ; blue background
  ld e,7 ; white foreground
  call clearscreen
  ld de,$00
  call putat
  ld hl,compare_pass_string
  call putstring
  call waitkey
  call waitreleasekey
  ret

.memtest_page_string
  defb "Testing ram page ",$00

.memtest
  ld d,1 ; blue background
  ld e,7 ; white foreground
  call clearscreen

  call memtest_page
  ret c

  ld de,$00
  call putat
  ld hl,compare_pass_string
  call putstring
  call waitkey
  call waitreleasekey
  ret

.memtest_page
  xor a
  call memtest_byte
  jr c,memtest_fail

  ld a,$ff
  call memtest_byte
  jr c,memtest_fail

  ld a,$aa
  call memtest_byte
  jr c,memtest_fail

  ld a,$55
  call memtest_byte
  jr c,memtest_fail

  or a
  ret

.memtest_fail
  push hl
  ld de,$00
  call putat
  ld hl,compare_fail_string
  call putstring
  pop hl
  push hl
  ld a,h
  call puthex
  pop hl
  ld a,l
  call puthex
  call waitkey
  call waitreleasekey
  scf
  ret

.memtest_byte
  ; first 00
  ld hl,$8000
  ld de,$8001
  ld bc,$7fff
  ld (hl),a
  ldir

; Delibrately fail for testing purposes
;  push af
;  ld a,$dd
;  ld ($c000),a
;  pop af

  ld hl,$8000
  ld b,$80
.compare_loop_1
  push bc
  ld b,$00
.compare_loop_2
  cp (hl)
  jr  nz,compare_fail
  inc hl
  djnz compare_loop_2
  pop bc
  djnz compare_loop_1
  or a
  ret

.compare_fail
  pop bc
  scf
  ret

.compare_fail_string
  defb "Failed at location $",$00

.compare_all_fail_string
  defb "Failed in bank $",$00

.compare_pass_string
  defb "Passed ram test",$00
