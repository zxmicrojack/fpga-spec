; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
.sdreset
  ret

  ; de - block number
  ; hl - address to load to
.sdread_block
  push af
  ld a,e
  cp $00
  jr  z,block0
  cp $01
  jr  z,block1

  push hl
  pop de
  inc de
  ld (hl),$de
  ld bc,511
  ldir
  pop af
  ret

.block0
  push hl
  pop de
  ld hl,block0_data
  ld bc,512
  ldir
  pop af
  ret

.block1
  push hl
  pop de
  ld hl,block1_data
  ld bc,512
  ldir
  pop af
  ret


.block0_data
  defb "bios                   ", "d", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 01           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 01           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 02           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 02           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 03           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 03           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 04           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 04           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 05           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 05           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 06           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 06           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 07           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 07           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 08           ", "t", $02, $00, $00, $00, $00, $04, $00, $00

.block1_data
  defb "Game disk 08           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 09           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 09           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 10           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 10           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 11           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 11           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 12           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 12           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 13           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 13           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 14           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 14           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game tape 15           ", "t", $02, $00, $00, $00, $00, $04, $00, $00
  defb "Game disk 15           ", "o", $02, $00, $00, $00, $00, $04, $00, $00
  defb $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe
  defb $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe, $fe
