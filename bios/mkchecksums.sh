#!/bin/bash
cp ../data/bios.dat roms.bin
dd if=/dev/zero bs=1024 count=16 >> roms.bin
split -b32768 roms.bin
cp xaa rampage1.bin
cp xab rampage2.bin
cp xac rampage3.bin
md5sum rampage1.bin rampage2.bin rampage3.bin > rampages.md5.new

diff rampages.md5 rampages.md5.new
rm xa?
