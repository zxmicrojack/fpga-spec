; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
.sdreset
  push bc
  push af
  ; read the status register
.sdreset_loop
  ld bc,SD_FIFO_STATE
  in a,(c)

  ; wait for interface to become ready after reset
  ; dont do - doesn't seem to ever become ready post boot
  ;bit 5,a
  ;jr z,sdreset_loop

  ; reset fifo
  ld a, SD_CMD_RESET
  out (c),a

  ; is fifo already empty
  in a,(c)
  bit 7,a
  jr nz,sdreset_exit

  ; go back and check it's fine
  jr sdreset_loop
.sdreset_exit
  pop af
  pop bc
  ret

  ; de - block number
  ; hl - address to load to
.sdread_block
  push af
  push bc
  push de

  ; reset fifo
.sdread_block_reset_fifo
  ld bc,SD_FIFO_STATE
  ld a, SD_CMD_RESET
  out (c),a

  ; is fifo already empty
  in a,(c)
  bit 7,a
  jr z,sdread_block_reset_fifo

  ; shift block number along 1 -> doing a * 512
  xor a
  rl e
  rl d
  rl a

  ; load block offset into registers
  ld bc,SD_ADDR_31_24
  out (c),a
  inc b ; addr_23_16
  ld a,d
  out (c),a
  inc b ; addr_15_8
  ld a,e
  out (c),a

  ld bc,SD_CMD ; perform read
  ld a,SD_CMD_READ
  out (c),a

  ld bc,SD_FIFO_STATE
.sdread_wait ; wait for data to appear
  in a,(c)
  bit 6,a
  jr z,sdread_wait

  ; data has appeared, now read it all out
  ld bc,SD_FIFO_READ
  ld de,512
.sdread_readloop
  in a,(c)
  ld (hl),a
  inc hl
  dec de

  xor a
  cp e
  jr nz,sdread_readloop
  cp d
  jr nz,sdread_readloop

  ; now reset the statemachine and end
  ld bc,SD_FIFO_STATE
  xor a
  out (c),a

  pop de
  pop bc
  pop af

  ret
