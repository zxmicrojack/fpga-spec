; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
org 28672

INCLUDE "common.asm"

  call memtest

  call memtest_all
  ret

INCLUDE "menus.asm"
INCLUDE "screen.asm"
INCLUDE "memtest.asm"
