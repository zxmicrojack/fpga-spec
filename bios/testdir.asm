; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
org 28672

INCLUDE "common.asm"

extern read_directory_from_sdcard
extern pagemem
  ld sp,28671

  ld a,4
  call pagemem

  ld a,'t'
  call read_directory_from_sdcard
  reti

  .videopos   defb $00,$00
  .dirtext  defb $00,$00
  .dirinfo  defb $00,$00
  .fsscratch_area  defs 512

INCLUDE "bootloader.asm"
INCLUDE "screen.asm"
INCLUDE "sdcard.asm"
INCLUDE "filesystem.asm"
