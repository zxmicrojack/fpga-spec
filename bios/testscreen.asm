; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
INCLUDE "common.asm"

  org 28672

extern putchar
extern putat
extern putstring
extern clearscreen
extern specscreen

  call specscreen
  call presskey
  call testputat
  call presskey
  call testsetcolours
  call presskey
  call testputstring
  call presskey
  call testputchar
  call presskey
  call testgetkey
  ret


.presskey
  call waitkey
  call waitreleasekey
  ret

.testgetkey
  ld d,0
  ld e,0
  call putat

  ld b,0

.testgetkey_loop
  push bc
  push de
  call waitkey
  call getkey
  push af
  call waitreleasekey
  pop af
  pop de
  call putchar
  pop bc
  djnz testgetkey_loop
  ret

.testwaitkey
  call waitkey
  ret

.testsetcolours
  ld d,2 ; red background
  ld e,7 ; white foreground
  call clearscreen
  ; deliberate fall through

.testputstring
  ld d,10
  ld e,10
  call putat

  ld hl,teststring
  call putstring
  ret
.teststring DEFB "Hello world!\0"

;A at each corner
.testputat
  ld d,0
  ld e,0
  call putat

  ld a,65
  call putchar

  ld d,23
  ld e,0
  call putat

  ld a,65
  call putchar

  ld d,23
  ld e,31
  call putat

  ld a,65
  call putchar

  ld d,0
  ld e,31
  call putat

  ld a,65
  call putchar

  ret

;fill with As
.testputchar
  ld d,0
  ld e,0
  call  putat


  ld b,3
.loop1
  push bc
  ld b,0
.loop2
  ld a,65
  call  putchar
  djnz loop2
  pop bc
  djnz loop1

  ret

INCLUDE "screen.asm"
