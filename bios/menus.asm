; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
#define HL 2
#define TITLEATTR $38

; #define BG 1
; #define FG 7
; #define NOSELITEMATTR $0f

#define BG 0
#define FG 7
#define NOSELITEMATTR $07

#define SELITEMATTR $17
#define LMARGIN 5


.menu
  ld bc,0
.menuloop
  push hl
  push de
  push bc
  call dispmenu
  jr  nc,menufullpage

  ld a,23
  cp b
  jr nz,menu_notemptypage

  pop hl
  jr menu_forcepageup

.menu_notemptypage
  ld a,22
  or a ; clear carry flag
  sub b
  jr menucallmenupages

.menufullpage
  ld a,21

.menucallmenupages
  call menupage

  pop hl

  cp $ff
  jr nz,menuskip_pageup
  ; page up

.menu_forcepageup
  xor a
  cp h
  jr nz,menu_dopageup
  cp l
  jr z,menu_endpageup

.menu_dopageup
  or a ; clear carry flag
  ld bc,22
  sbc hl,bc

.menu_endpageup
  push hl
  pop bc
  pop de
  pop hl
  jr menuloop

.menuskip_pageup
  cp 22
  jr nz,menuskip_pagedown

  ; page down
  or a ; clear carry flag
  ld bc,21
  adc hl,bc

  push hl
  pop bc
  pop de
  pop hl
  jr menuloop

.menuskip_pagedown
  pop de
  pop de

  ld b,0
  ld c,a

  add hl,bc

  ret

.dispmenu_empty defb "No items to list.", $00, $00

.menustore_data defb $00,$00
.menustore_title defb $00,$00
.menustore_count defb $00,$00



; a = max items
.menupage
    ld h,a
    xor a
.menu_main
    ld b,SELITEMATTR
    call invitem
    push af
.menu_keyloop
    push hl
    call waitkey
    push af
    call waitreleasekey
    pop af
    pop hl

    ; 6 move down
    cp $36
    jr nz,nextkeyscan
    pop af         ; get index
    cp h
    jr nc,movedown1_dont

    ld b,NOSELITEMATTR       ; clear selection
    call invitem
    inc a          ; increment - TODO check limit
.movedown1_dont
    jr menu_main   ; redraw
.nextkeyscan
    cp $37 ; 7 move down
    jr nz,nextkeyscan1
    pop af         ; get index
    cp 0
    jr z,moveup1_dont

    ld b,NOSELITEMATTR       ; clear selection
    call invitem
    dec a          ; increment - TODO check limit
.moveup1_dont
    jr menu_main   ; redraw
.nextkeyscan1
    cp $38 ; 8 page down
    jr nz,nextkeyscan2

    ld a,h
    cp 21
    jr c,movepdown_dont

    pop af ; retrieve index
    ld a,22
    ret
.movepdown_dont
    pop af
    jr menu_main   ; redraw

.nextkeyscan2
    cp $35
    jr nz,nextkeyscan3
    pop af
    ld a,$ff
    ret
.nextkeyscan3
    cp $30
    jr nz,nextkeyscan4
    pop af
    ret
.nextkeyscan4
    jr menu_keyloop

; TODO optimise - very suboptimal
.invtitle
    push hl
    push de
    push bc
    ld hl,SCREEN_ATTR_POS
    push hl
    pop de
    inc de
    ld (hl),TITLEATTR
    ld bc,31
    ldir
    pop bc
    pop de
    pop hl
    ret

.invitem
    push af ; preserve index
    push hl ; preseve hl

    inc a
    inc a

    ; hl = a * 32 + 16384 + 6144
    and $1f
    ld l,a
    ld h,0
    or a ; clear carry flag
    sla l
    sla l
    sla l
    sla l
    rl h
    sla l
    rl h
    ld a,$58
    or a,h
    ld h,a

    ; set attributes on row
    ld a,5
    or l
    ld l,a
    ld a,b
    ld (hl),b
    push hl
    pop de
    inc de
    ; ld bc,31
    ld bc,21
    ldir


    pop hl ; preseve hl
    pop af ; retrieve index
    ret

  .dispmenu
    push bc
    push hl
    push de

    ; ld d,BG
    ; ld e,FG
    ; call clearscreen
    call specscreen

    pop hl

    ; xor a
    ; TODO optimise this
    ld de,SCREEN_PIXEL_POS + LMARGIN
    call putstring

    ; ld bc,0
    ; call dispmenuitems

    pop hl
    pop bc
    ld a,2
    call dispmenuitems
    push bc

    call invtitle
    pop bc

    ret


    ; offset y by a
  .dispmenuitems
    push af
    ; offset items by BC, but dont skip if bc == 0
    xor a
    cp b
    jr  nz,dispmenu_loop
    cp c
    jr nz,dispmenu_loop
    jr dispmenu_skipped

    ; jump over items
  .dispmenu_loop
    inc hl
    cp (hl)
    jr  nz,dispmenu_loop
    inc hl

    cp (hl) ; check if is last item
    jr z,dispmenu_exit_eof_skipped

    dec bc
    cp b
    jr nz,dispmenu_loop
    cp c
    jr nz,dispmenu_loop

  .dispmenu_skipped
    pop af
    ld d,a
    ld e,LMARGIN
    ld b,22
  .dispmenuloop
    push bc
    push de
    push hl
    call putat
    pop hl
    call putstring
    pop de
    inc d
    inc hl
    pop bc
    xor a
    cp (hl)
    jr z,dispmenu_exit_eof
    djnz dispmenuloop
    or a ; clear carry flag
.dispmenu_exit
    ret
.dispmenu_exit_eof_skipped
    pop af
    scf
    ld b,23
    ret
.dispmenu_exit_eof
    scf
    ret

; hl = title of operation
; de = max blocks
.progress
  push de
  push hl
  call specscreen

  ld de,SCREEN_PIXEL_POS + LMARGIN
  pop hl
  call putstring
  call invtitle

  pop de

  ld b,5
.progress_calc
  or a
  srl d
  rr e
  djnz progress_calc

  ld (progress_seg),de
  dec de
  ld (progress_count),de
  ld de,SCREEN_ATTR_POS + 12*32
  ld (progress_pos),de
  ret

.progress_update
  push hl
  push de
  ld hl,(progress_count)
  dec hl
  ld (progress_count),hl
  bit 7,h
  jr z,progress_update_end

  ld de,(progress_seg)
  dec de
  ld (progress_count),de

  ld hl,(progress_pos)
  ld (hl),$ff
  inc hl
  ld (progress_pos),hl

.progress_update_end
  pop de
  pop hl
  ret
