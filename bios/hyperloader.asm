; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
INCLUDE "common.asm"

  org $0556

.load
  inc d
  ex af,af
  dec d

  di

  ; make entrypoint for other games that don't want to use the
  ; return procedure
  nop
  nop
  nop
  nop

  ; push SA/LD-RET routine to return to
  ld hl,$053f
  push hl

  ; some games come in here to load
  ld bc,TAP_HYPERLOAD_RESET

  ; get block size
  in a,(c) ; lsb of block size
  ld l,a
  in a,(c) ; msb of block size
  ld h,a

  ; compare expected lengths
  inc de
  inc de
  ld a,h
  cp d
  jr nz,wasteloop
  ld a,l
  cp e
  jr nz,wasteloop

  ; DE no longer needed - is duplicate of HL
  ; decrement and fetch flag byte
  dec hl
  ;dec hl
  in a,(c) ; type of block

  ; does it match with expected
  ld d,a
  ex af,af'
  cp d
  jr nz,wasteloop


  dec hl ; don't read parity byte at the end into memory

.loop
  in a,(c)
  ld (ix),a
  inc ix
  dec hl

  ; compare hl to 0
  xor a
  cp h
  jr nz,loop
  cp l
  jr nz,loop

  in a,(c) ; swallow the parity byte
  scf
  ret

.wasteloop
  in a,(c)
  dec hl

  ; compare hl to 0
  xor a
  cp h
  jr nz,wasteloop
  cp l
  jr nz,wasteloop

  scf
  ccf
  ret
