; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo
INCLUDE "common.asm"

  org 28672

extern load_roms_from_sdcard
  ld sp,28671
  call load_roms_from_sdcard

  ; copy page switching to ram and run
  ld hl,reset
  ld de,16384
  ld bc,resetend-reset
  ldir
  jp 16384

.reset
  ; switch to 48k spectrum rom
  di
  ; page in spectrum rom - TODO move to RAM
  ld bc,$05ff
  ld a,$02
  out (c),a
  reti
  ; shouldn't get here
.resetend



  ; .videopos   defb $00,$00
  ; .dirtext  defb $00,$00
  ; .dirinfo  defb $00,$00

INCLUDE "bootloader.asm"
INCLUDE "screen.asm"
INCLUDE "sdcard.asm"
INCLUDE "filesystem.asm"
INCLUDE "menus.asm"
INCLUDE "fakescratch.asm"

; .fsscratch_area defs 512
