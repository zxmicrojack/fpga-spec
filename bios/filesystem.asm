; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo

.read_directory_from_sdcard
; a-> type of file to list
  ld bc,DIRTEXT_POS
  ld (dirtext),bc

  ld bc,DIRINFO_POS
  ld (dirinfo),bc

  ld de,0

.dir_block_read_loop
  push de

  ; TODO: remove references to bad memory
  ld hl,fsscratch_area
  call sdread_block
  ld b,512/32
  ld ix,fsscratch_area

.dir_entry_loop
  push af

  ld a,$fe
  cp (ix+23)
  jr z,read_directory_from_sdcard_end

  pop af

  cp (ix+23)
  jr nz,not_filetype

  ; is a file of right type
  push bc
  ld de,(dirtext)
  push ix
  pop hl
  ld bc,23
  ldir

  ld de,24
  push ix
  pop hl
  add hl,de
  ld de,(dirinfo)
  ld bc,8
  ldir

  ld de,(dirtext)
  ld hl,22
  add hl,de
  ld (hl),$00
  inc hl
  push hl
  pop de
  ld (dirtext),de

  ld de,(dirinfo)
  ld hl,8
  add hl,de
  push hl
  pop de
  ld (dirinfo),de
  pop bc

.not_filetype

  push bc
  ld bc,32
  add ix,bc
  pop bc
  djnz dir_entry_loop
  pop de
  inc de
  jr  dir_block_read_loop

.read_directory_from_sdcard_end
  pop af
  pop de
  ; add extra null terminator
  ld hl,(dirtext)
  ld (hl),$00
  ret

; ix -> file record
; hl -> text
; a -> starting ram page
.read_file_from_sdcard
  push af
  push hl

  ld e,(ix+0) ; block number
  ld d,(ix+1) ; block number

  ld h,(ix+5) ; lower 16 bits of size field
  ld l,(ix+4)
  ld a,$01
  and h
  ld h,a
  ld a,$ff
  and l
  ld l,a ; hl contains number of bytes in last block

  ld a,(ix+7) ; convert bytes to blocks
  ld b,(ix+6)
  ld c,(ix+5)

  or a ; reset carry flag
  rr a
  rr b
  rr c

  ; add on one block if number of bytes in last block != 0
  xor a
  cp  h
  jr  nz,read_file_from_sdcard_addblock
  cp  l
  jr  nz,read_file_from_sdcard_addblock
  jr read_file_from_sdcard_doblocks

.read_file_from_sdcard_addblock
  inc bc

.read_file_from_sdcard_doblocks
  ; now show progress screen
  pop hl

  push de

  push bc
  push bc
  pop de
  call progress
  pop bc
  pop de

  ; load roms
  pop af
;  ld a,1 ; first ram page
  call pagemem
  push af
  ld hl,32768

._loadrompage

._loadblock
  call sdread_block
;  call output_progress
  call progress_update

  inc de ; inc block number
  dec bc ; dec block count

  ; has finished writing this page, then page new
  xor a
  cp  h
  jr nz,_loadrom_dontpage
  ld hl,32768
  pop af
  inc a
  call pagemem
  push af

  ; either finished paging or didn't need paging
  ; have we done reading - if so finished
._loadrom_dontpage
  xor a
  cp  b
  jr nz,_loadrompage
  cp  c
  jr nz,_loadrompage
  pop af

  ; restore ram page
  xor a
  call pagemem
  ret

.pagemem
  push bc
  ld bc,MEM_PAGING_REG
  out (c),a
  pop bc
  ret
