; This file is part of fpga-spec by ZXMicroJack - see LICENSE.txt for moreinfo

#define DIRTEXT_POS $8200
#define DIRINFO_POS $8200 + 1000 * 24

#define SD_FIFO_STATE $00ff
#define SD_FIFO_READ $01ff
#define SD_CMD $00ff
#define SD_ADDR_31_24 $01ff
#define SD_ADDR_23_16 $02ff
#define SD_ADDR_15_8 $03ff
#define MEM_PAGING_REG $4ff

#define SD_CMD_RESET    2
#define SD_CMD_READ     1

#define SD_FIFO_EMPTY   0x80

#define TAPSIZE_17_16 $6ff
#define TAPSIZE_15_8 $7ff
#define TAPSIZE_7_0 $8ff

#define DISKSIZE_19_16 $aff
#define DISKSIZE_15_8 $bff
#define DISKSIZE_7_0 $cff
#define TAP_HYPERLOAD_RESET $dff

#define MACHINE_TYPE  $09ff
#define MACHINE_RESET_POS   $0120
#define SCREEN_ATTR_POS 22528
#define SCREEN_PIXEL_POS 16384

#define MEMPAGE_DISK 224
#define MEMPAGE_TAPE 192
#define MEMPAGE_DIRSCRATCH   $0a

; rom pages are 1->3
; spec 48 ram is 0,9a
; spec 128 ram is 4,5,6a,9b
; peripheral ram is 8
; dir scratch area is 10
